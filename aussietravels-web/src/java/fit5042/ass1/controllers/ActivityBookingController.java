/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingSearch;
import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.Resorts;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * Controller for activity details page
 * @author Jishnu
 */
@Named(value = "activityBookingController")
@RequestScoped
public class ActivityBookingController {

    
    
    
    private String pageTitle;
    private String bookingId;
    private ActivtyBooking activityBooking;
    private Activity activity;
    
    @EJB
    private BookingSearch bookingSearchImpl;
    
    
    
    /**
     * Creates a new instance of ActivityBookingController
     */
    public ActivityBookingController() {
    }
    
    
    @PostConstruct
    public void init() {
    
        //TODO : PUT TRY EXCEPT AND ACCESS CONTROL 
        bookingId = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("activityBookingId");
        
             activityBooking = getBooking();
         
         if (activityBooking != null) {
         
             activity = activityBooking.getActivityId();
            
         }
         
         
         if (activity != null && activityBooking != null) {
         
             pageTitle = activity.getActivityName().toUpperCase() + " BOOKING" ;
         
         }
    
    }
    
    
    
    
    
     /*
    * This method returns the activity booking from the EJB based on the booking id
    * @Param :  
    * @Return : Activity Booking
    */
    private ActivtyBooking getBooking() {
    
           ArrayList<ActivtyBooking> activityList = new ArrayList<ActivtyBooking>();
           activityList =  bookingSearchImpl.searchActivityById(bookingId);
        
        if (activityList.isEmpty()) {
        
            return null;
        }
        else {
        
            return activityList.get(0);
        }
    
    
    
    }
    
    
    
    // getters and setters
    
    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public ActivtyBooking getActivityBooking() {
        return activityBooking;
    }

    public void setActivityBooking(ActivtyBooking activityBooking) {
        this.activityBooking = activityBooking;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    

    
    
    
    
}
