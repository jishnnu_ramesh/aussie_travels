
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingSearch;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.Resorts;
import fit5042.ass1.resort.ResortDAO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * Controller for resort booking details page
 * @author Jishnu
 */
@Named(value = "resortBookingController")
@RequestScoped
public class ResortBookingController {

    

    private String pageTitle;
    private String bookingId;
    private ResortBooking resortBooking;
    private Resorts resort;
    private double airplaneCharges;
    
    @EJB
    private BookingSearch bookingSearchImpl;
    

    
    
    /**
     * Creates a new instance of ResortBookingController
     */
    public ResortBookingController() {
            
    }
    
    
   @PostConstruct
   public void init(){
    
       //TODO : PUT TRY EXCEPT AND ACCESS CONTROL 
        bookingId = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("resortBookingId");
        
         resortBooking = getBooking();
         
         if (resortBooking != null) {
         
             resort = resortBooking.getResortId();
            
         }
         
         
         if (resort != null && resortBooking != null) {
         
             pageTitle = resort.getResortName() + " RESORT BOOKING" ;
         
            if (resortBooking.getTotalCost().doubleValue() != 0.0) {
             airplaneCharges = resortBooking.getTotalCost().doubleValue() - ( resortBooking.getRoomType().getRoomCost().doubleValue() * 
                    resortBooking.getNumOfPeople()  );
            }
            else {
            
                airplaneCharges = 0.0;
            }
         }
        
    } 
    

   
   
    /*
    * This method returns all the resort booking based on the booking Id
    * @Param : 
    * @Return : Resort Booking
    */
    private ResortBooking getBooking() {
    
        ArrayList<ResortBooking> resortList = new ArrayList<ResortBooking>();
        resortList =  bookingSearchImpl.searchResortById(bookingId);
        
        if (resortList.isEmpty()) {
        
            return null;
        }
        else {
        
            return resortList.get(0);
        }
        
    }
    
    
    
    
    // Getters and setters
   
    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public ResortBooking getResortBooking() {
        return resortBooking;
    }

    public void setResortBooking(ResortBooking resortBooking) {
        this.resortBooking = resortBooking;
    }

    public Resorts getResort() {
        return resort;
    }

    public void setResort(Resorts resort) {
        this.resort = resort;
    }

    public double getAirplaneCharges() {
        return airplaneCharges;
    }

    public void setAirplaneCharges(double airplaneCharges) {
        this.airplaneCharges = airplaneCharges;
    }
    
    

    
}

