/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.activity.ActivityDAO;
import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.Packages;
import fit5042.ass1.entities.Resorts;
import fit5042.ass1.entities.Users;
import fit5042.ass1.packages.PackagesDAO;
import fit5042.ass1.resort.ResortDAO;
import fit5042.ass1.util.ActivityResultList;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 */
@Named(value = "searchController")
@SessionScoped
public class SearchController implements Serializable {

    
    @EJB
    private ResortDAO resortDAOImpl;
    
    @EJB
    private PackagesDAO packageDAOImpl;
    
    @EJB
    private ActivityDAO activityDAOImpl;
    

    private String PageTitle;
    private Map<String,String> resortsLocations;
    private Map<String,String> packageNames;
    private Map<String,String> activityNames;
    
    
    // For temp data storage
    private ArrayList<Packages> packages;
    private ArrayList<Resorts> resorts;
    private ArrayList<Activity> activity;
    
    private String resortLocationSelected;
    private String packageNameSelected;
    private String activityNameSelected;

   
    public SearchController() {
        
        PageTitle = "Search";
        resortsLocations = new HashMap<String, String>();
        packageNames = new HashMap<String, String>();
        activityNames  = new HashMap<String, String>();
        
        packages = new ArrayList<Packages>();
        resorts = new ArrayList<Resorts>();
        activity = new ArrayList<Activity>();
        
        
        
    }

   
    
    
    @PostConstruct
    public void init() {
    

        resorts = resortDAOImpl.getAllResorts();
        
        for(Resorts r : resorts)
        {
        
            resortsLocations.put(r.getResortName(), String.valueOf(r.getResortId()));
        
        
        }
        
        packages = packageDAOImpl.getAllPackages();
        
        for (Packages p : packages) {
        
            packageNames.put(p.getPackageName(), String.valueOf(p.getPackageId()));
        
        }
        
        
        activity = activityDAOImpl.getAllActivities();
        
        for (Activity a : activity) {
        
            activityNames.put(a.getActivityName(), String.valueOf(a.getActivityId()));
        
        }

        }
    
  
  /*
    * This method redirects user to the page to book resorts
    * @Param : 
    * @Return : 
    */
 public void  bookResorts()
 {
 
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/bookResorts.xhtml?bookResorts=" + resortLocationSelected;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
 
 
 }   
 
 
 
 /*
    * This method redirects user to the page to book packages
    * @Param : 
    * @Return : 
    */
 public void bookPackages() {
 
 
      String url = "https://localhost:11488/aussietravels-web/faces/public/bookPackages.xhtml?package=" + packageNameSelected;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
 
 
 }
 
 
 /*
    * This method redirects user to the page to book activities
    * @Param : 
    * @Return : 
    */
  public void bookActivity() {
 
 
      String url = "https://localhost:11488/aussietravels-web/faces/public/bookActivities.xhtml?activity=" + activityNameSelected;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
 
 
 }
    
    
    
    
  // getters and setters
    
 public String getPageTitle() {
        return PageTitle;
    }

    public void setPageTitle(String PageTitle) {
        this.PageTitle = PageTitle;
    }

    public Map<String, String> getResortsLocations() {
        return resortsLocations;
    }

    public void setResortsLocations(Map<String, String> resortsLocations) {
        this.resortsLocations = resortsLocations;
    }

    public Map<String, String> getPackageNames() {
        return packageNames;
    }

    public void setPackageNames(Map<String, String> packageNames) {
        this.packageNames = packageNames;
    }


    public Map<String, String> getActivityNames() {
        return activityNames;
    }

    public void setActivityNames(Map<String, String> activityNames) {
        this.activityNames = activityNames;
    }

    public ArrayList<Packages> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<Packages> packages) {
        this.packages = packages;
    }

    public ArrayList<Resorts> getResorts() {
        return resorts;
    }

    public void setResorts(ArrayList<Resorts> resorts) {
        this.resorts = resorts;
    }

    public ArrayList<Activity> getActivity() {
        return activity;
    }

    public void setActivity(ArrayList<Activity> activity) {
        this.activity = activity;
    }

    public String getResortLocationSelected() {
        return resortLocationSelected;
    }

    public void setResortLocationSelected(String resortLocationSelected) {
        this.resortLocationSelected = resortLocationSelected;
    }

    public String getPackageNameSelected() {
        return packageNameSelected;
    }

    public void setPackageNameSelected(String packageNameSelected) {
        this.packageNameSelected = packageNameSelected;
    }

  

    public String getActivityNameSelected() {
        return activityNameSelected;
    }

    public void setActivityNameSelected(String activityNameSelected) {
        this.activityNameSelected = activityNameSelected;
    }
    
    
    
    
    
    
      }
    
    
    
    
    
   
    
