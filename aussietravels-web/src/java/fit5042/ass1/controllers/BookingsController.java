
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingSearch;
import fit5042.ass1.bookingtype.BookingTypeDAO;
import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.BookingType;
import fit5042.ass1.entities.PackageBooking;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserSearch;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 * Controller for bookings search page 
 */
@Named(value = "bookingsController")
@RequestScoped
public class BookingsController implements Serializable {

    @EJB
    private BookingSearch bookingSearchImpl; 
    
    @EJB
    private BookingTypeDAO bookingTypeDaoImpl;
    
    
    @EJB
    private UserSearch userSearchImpl;
   
   private String pageTitle;
   private String searchByText;
   private String searchText;
   private String bookingTypeString;
   private Map<String,String> searchBy;
   private Map<String,String> bookingTypeDropDownText;
   private ArrayList<ActivtyBooking> activityResultList;
   private ArrayList<PackageBooking> packageResultList;
   private ArrayList<ResortBooking> resortResultList;
   
   
   private boolean firstTimeLoad;
   private boolean resultAvailable;

    
    /**
     * Creates a new instance of BookingsController
     */
    public BookingsController() {
    // Set the page title 
        pageTitle = "Aussie Holidays";
        searchText = "";
        bookingTypeString = "0";
        activityResultList = new ArrayList<ActivtyBooking>();
        packageResultList = new ArrayList<PackageBooking>();
        resortResultList = new ArrayList<ResortBooking>();
        resultAvailable = false;
        firstTimeLoad = true;
    }

    

   
    
    @PostConstruct
    public void init(){
    
        searchBy = new LinkedHashMap<String,String>();
        bookingTypeDropDownText = new LinkedHashMap<String,String>();   
        searchBy.put("Booking Id", "1");
        searchBy.put("Booking Type", "2");
        searchBy.put("Booking Name", "3");
        getBookingTypes();
    }
    
    
     private void getBookingTypes() {
     
         ArrayList<BookingType> bookingType = bookingTypeDaoImpl.getAllBookingTypes();
         
         if (bookingType != null && !bookingType.isEmpty()) {
         
             for (BookingType b : bookingType) {
             
                 bookingTypeDropDownText.put(b.getTypeName(), b.getBookingId().toString()) ;
             }
         
         }
     
     
     }
    
    
    
     /*
    * This method is respnsible for calling the ejb to search for bookings and
     the result will be set to the concerned variables
    * @Param : 
    * @Return : 
    */
    public void search() {
   
        firstTimeLoad = false;
        activityResultList = new ArrayList<ActivtyBooking>();
        packageResultList = new ArrayList<PackageBooking>();
        resortResultList = new ArrayList<ResortBooking>();
        
        ArrayList<ResortBooking> resortResult =  new ArrayList<ResortBooking>();
        ArrayList<PackageBooking> PackageResult = new ArrayList<PackageBooking>();
        ArrayList<ActivtyBooking> activityResult = new ArrayList<ActivtyBooking>();
        
        
         String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
         Users id = userSearchImpl.getUserByEmail(email);
             
        resultAvailable = false;
        
        if (searchByText.matches("1")) {
        
            if ( searchText.startsWith("a") || searchText.startsWith("A")) {
            
                String searchId = searchText.substring(1);
                 activityResult = bookingSearchImpl.searchActivityById(searchId);
                 
                 for (ActivtyBooking a : activityResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         activityResultList.add(a);
                         
                     }
                 
                 }
                 
            }
            
            else if (searchText.startsWith("p") || searchText.startsWith("P")) {
            
                String searchId = searchText.substring(1);
                PackageResult = bookingSearchImpl.searchPackageById(searchId);
                
                for (PackageBooking a : PackageResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         packageResultList.add(a);
                         
                     } 
                     
                 
                 }
                
            }
             
            else if (searchText.startsWith("r") || searchText.startsWith("R")) {
            
                String searchId = searchText.substring(1);
                resortResult = bookingSearchImpl.searchResortById(searchId);
                for (ResortBooking a : resortResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         resortResultList.add(a);
                         
                     } 
                     
                 
                 }
                
                
            }
            
            
        }
        
        
        else if (searchByText.matches("2")) {
        
            try{
            
                int searchId = Integer.parseInt(bookingTypeString);
                BookingType bookingType = bookingTypeDaoImpl.getBookingType(searchId);
                searchText = bookingTypeDropDownText.get(bookingTypeString);
                if (bookingType != null) {
                    
                    resortResult = bookingSearchImpl.searchResortByType(bookingType);
                    PackageResult = bookingSearchImpl.searchPackageByType(bookingType);
                    activityResult = bookingSearchImpl.searchActivityByType(bookingType);
                    for (ActivtyBooking a : activityResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         activityResultList.add(a);
                         
                     }
                 
                 }
                    
                    for (PackageBooking a : PackageResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         packageResultList.add(a);
                         
                     } 
                     
                 
                 }
                    
                    for (ResortBooking a : resortResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         resortResultList.add(a);
                         
                     } 
                     
                 
                 }
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
            }
            catch (Exception ex) {
             
                resultAvailable = false;
                return;
                
            }
        
        }
            
        
        
       
        else if (searchByText.matches("3")) {
        
            searchText = searchText.toUpperCase();
            
            if (searchText.startsWith("RESORT BOOKING")) {
            
                resortResult = bookingSearchImpl.searchResortByTransactionName(searchText);
                
                
                for (ResortBooking a : resortResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         resortResultList.add(a);
                         
                     } 
                     
                 
                 }
                
            }
            
            else if (searchText.startsWith("PACKAGE BOOKING")) {
            
            
                PackageResult = bookingSearchImpl.searchPackageByTransactionName(searchText);
                
                for (PackageBooking a : PackageResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         packageResultList.add(a);
                         
                     } 
                     
                 
                
                
            }
            }
            
            else if (searchText.startsWith("ACTIVITY BOOKING")) {
            
            
                activityResult = bookingSearchImpl.searchActivityByTransactionName(searchText);
                    
                    for (ActivtyBooking a : activityResult) {
                 
                     if  (Objects.equals(a.getUserId().getUserId(), id.getUserId())) {
                     
                         activityResultList.add(a);
                         
                     }
                 
                 }
                    
        
            }
            
            
            
            
        }
        
        
        resultAvailable = !(activityResultList.isEmpty() &&
                packageResultList.isEmpty() && 
                resortResultList.isEmpty());
        
        
    
    }
    

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    
    
    public Map<String, String> getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(Map<String, String> searchBy) {
        this.searchBy = searchBy;
    }

    
      public String getSearchByText() {
        return searchByText;
    }

    public void setSearchByText(String searchByText) {
        this.searchByText = searchByText;
    }
    
    
     public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    
    
     public boolean isResultAvailable() {
        return resultAvailable;
    }

    public void setResultAvailable(boolean seting) {
        this.resultAvailable = seting;
    }
    
    
    public ArrayList<ActivtyBooking> getActivityResultList() {
        return activityResultList;
    }

    public void setActivityResultList(ArrayList<ActivtyBooking> activityResultList) {
        this.activityResultList = activityResultList;
    }

    public ArrayList<ResortBooking> getResortResultList() {
        return resortResultList;
    }

    public void setResortResultList(ArrayList<ResortBooking> resortResultList) {
        this.resortResultList = resortResultList;
    }
    
      public ArrayList<PackageBooking> getPackageResultList() {
        return packageResultList;
    }

    public void setPackageResultList(ArrayList<PackageBooking> packageResultList) {
        this.packageResultList = packageResultList;
    }
    
    public boolean isFirstTimeLoad() {
        return firstTimeLoad;
    }

    public void setFirstTimeLoad(boolean firstTimeLoad) {
        this.firstTimeLoad = firstTimeLoad;
    }

    public Map<String, String> getBookingTypeDropDownText() {
        return bookingTypeDropDownText;
    }

    public void setBookingTypeDropDownText(Map<String, String> bookingTypeDropDownText) {
        this.bookingTypeDropDownText = bookingTypeDropDownText;
    }
    
     public String getBookingTypeString() {
        return bookingTypeString;
    }

    public void setBookingTypeString(String bookingTypeString) {
        this.bookingTypeString = bookingTypeString;
    }
    
}
