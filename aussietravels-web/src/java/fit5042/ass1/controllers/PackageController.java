/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingDAO;
import fit5042.ass1.bookingtype.BookingTypeDAO;
import fit5042.ass1.entities.Packages;
import fit5042.ass1.entities.Users;
import fit5042.ass1.packages.PackagesDAO;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jishnu
 */
@Named(value = "packageController")
@SessionScoped
public class PackageController implements Serializable {

   

  
    private String packageID;
    
    private Packages packages;
    
    private HashMap<String,String> bookingTypeDropDown;

   
    private HashMap<String,String> numberOfPeople;
   
    @NotNull
    private String bookingName;

    private double totalPrice;
 
    @NotNull
    private String noOfPeopleText;
    
    private String bookingTypeSelection;

   
    @EJB
    private UserSearch userSearchImpl;
    
    
    @EJB
    private PackagesDAO packageDAOImpl;
    
    
    @EJB
    private BookingTypeDAO bookingTypeDAOImpl;
    
    
    @EJB
    private BookingDAO bookingDAOImpl;
    
    
    @NotNull
    private String date;

    
    
    
    /**
     * Creates a new instance of PackageController
     */
    public PackageController() {
    }
    
    
    @PostConstruct
    public void init() {
    
             if (packageID == null || packageID.length() == 0) {
        
            packageID = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("package"); 
            
            //System.out.println(packageID);
            
        if (packageID == null || packageID.length() == 0) {
            
            redirectToError();

       }
          
 
    }
         
         populateData();
         
        
         
    }
    
    
    
     /*
    * This method populate the data for all binded fields in the JSF
    * @Param : 
    * @Return : 
    */
    public void populateData() {
    
         numberOfPeople = new HashMap<>();
         bookingTypeDropDown = new HashMap<>();
         numberOfPeople.put("1","1");
         numberOfPeople.put("2","2");
         numberOfPeople.put("3","3");
         numberOfPeople.put("4","4");
         numberOfPeople.put("5","5");
         numberOfPeople.put("6","6");
         numberOfPeople.put("7","7");
         numberOfPeople.put("8","8");
         numberOfPeople.put("9","9");
         
         noOfPeopleText = "1";
         
         packages = packageDAOImpl.findPackageById(packageID);
    
    
        bookingTypeDropDown =  bookingTypeDAOImpl.getAllBookingTypesFormWebService();
        
        
        totalPrice = packages.getPackageCost().doubleValue();
    
         date = "";
    
    
    
    
    }
    
    
    
     /*
    * This method recalculate the total cost based on any 
    chnage in the user selection in the booking page. This facilitates the 
    Ajax calls
    * @Param : 
    * @Return : 
    */
    public void valueChange() {
    
        totalPrice = Integer.parseInt(noOfPeopleText) * packages.getPackageCost().doubleValue();
    
    
    }
    
    
    
    
     /*
    * This method redirects user to the correct payment screen based on the
    booking type
    * @Param : 
    * @Return : 
    */
    
    public void pay() {
    
    if (bookingTypeSelection.equals("1")) {
        
       String url = "https://localhost:11488/aussietravels-web/faces/public/payment.xhtml?type=packages";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      else if (bookingTypeSelection.equals("2")) {
      
      
          String url = "https://localhost:11488/aussietravels-web/faces/public/voucherPayment.xhtml?type=packages";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      }
    
    }
    
    
    
    
     /*
    * This method clears the state of all variables on the session bean after 
    a sucessfull booking
    * @Param : 
    * @Return : 
    */
    public long modelStateClear() {
        
     String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
     Users currentUser = userSearchImpl.getUserByEmail(email);
    
    
    long id = bookingDAOImpl.savePackageBooking(bookingName, packages, noOfPeopleText, bookingTypeSelection, totalPrice, currentUser, date);
     
    
    bookingName = "";
    noOfPeopleText = "";
    bookingTypeSelection = "";
    packages = null;
    totalPrice = 0; 
     date = "";
     
     return id;
  
    }
    
    
    
         
     /*
    * This method redirects the user to an error page if any error occurs
    * @Param : 
    * @Return :
    */
    public void redirectToError() {
    
        String url = "https://localhost:11488/aussietravels-web/faces/public/errpr.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }   
         
    
   
         
     // Getters and setters    
    
     public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public Packages getPackages() {
        return packages;
    }

    public void setPackages(Packages packages) {
        this.packages = packages;
    }

    public HashMap<String, String> getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(HashMap<String, String> numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

   

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNoOfPeopleText() {
        return noOfPeopleText;
    }

    //@EJB
    public void setNoOfPeopleText(String noOfPeopleText) {
        this.noOfPeopleText = noOfPeopleText;
    }
    
         
      public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }
         
    
     public HashMap<String, String> getBookingTypeDropDown() {
        return bookingTypeDropDown;
    }

    public void setBookingTypeDropDown(HashMap<String, String> bookingTypeDropDown) {
        this.bookingTypeDropDown = bookingTypeDropDown;
    }
    
    
     public String getBookingTypeSelection() {
        return bookingTypeSelection;
    }

    public void setBookingTypeSelection(String bookingTypeSelection) {
        this.bookingTypeSelection = bookingTypeSelection;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

   
    
    
    
    
}
