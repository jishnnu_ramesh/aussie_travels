/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingSearch;
import fit5042.ass1.entities.PackageBooking;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.Packages;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * Controller for package bookings details page
 * @author Jishnu
 */
@Named(value = "packageBookingController")
@RequestScoped
public class PackageBookingController {

   

    
    private String pageTitle;
    private String bookingId;
    private PackageBooking packageBooking;
    private Packages packages;
    
    @EJB
    private BookingSearch bookingSearchImpl;
    
    
    
    /**
     * Creates a new instance of PackageBookingController
     */
    public PackageBookingController() {
    }
    
    
    @PostConstruct
    public void init() {
    
        //TODO : PUT TRY EXCEPT AND ACCESS CONTROL AND ACCESS CONTROL 
        bookingId = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("packageBookingId");
        
         packageBooking = getBooking();
         
         if (packageBooking != null) {
         
             packages = packageBooking.getPackageId();
            
         }
         
         
         if (packages != null && packageBooking != null) {
         
             //TODO : CHANGE THE NAME
             pageTitle = "PACKAGE RESERVATION DETAILS";
         
         }
    
    
    }
    
    
    
     /*
    * This method returns all the package booking based on the booking Id
    * @Param : 
    * @Return : Package Booking
    */
    private PackageBooking getBooking() {
    
        ArrayList<PackageBooking> packageList = new ArrayList<PackageBooking>();
        packageList =  bookingSearchImpl.searchPackageById(bookingId);
        
        if (packageList.isEmpty()) {
        
            return null;
        }
        else {
        
            return packageList.get(0);
        }
    
    
    }
    
    
    
    
    
    
    // getters and setters
    
     public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public PackageBooking getPackageBooking() {
        return packageBooking;
    }

    public void setPackageBooking(PackageBooking packageBooking) {
        this.packageBooking = packageBooking;
    }

    public Packages getPackages() {
        return packages;
    }

    public void setPackages(Packages packages) {
        this.packages = packages;
    }

    
    
    
    
}
