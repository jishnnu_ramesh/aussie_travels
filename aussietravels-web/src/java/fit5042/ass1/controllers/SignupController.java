/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.users.UserDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Jishnu
 */
@Named(value = "signupController")
@RequestScoped
public class SignupController {

    
    private final String LOGIN_ERROR = "Error : Your registration was not processed";
  
    
    @EJB
    private UserDAO userDAOImpl;
    
    @NotNull
    //@Size(min = 2, max = 30,  message = " Last Name should be at least 2 letters long")
    private String firstName;
    
    @NotNull
   // @Size(min = 2, max = 30,  message = " Last Name should be at least 2 letters long")
    private String lastName;
    
    @NotNull
    @Size(min = 5, max = 30,  message = "invalid email")
    @Email
    private String email;
   
    @NotNull
   // @Size(min = 8 , max = 10, message = "Invalid Phone Number")
    //@Pattern(regexp = "0[0-9]{9}|9[0-9]{7}", message = "Should be a valid mobile or land phone number ")
    private String phoneNumber;
    
    
    @NotNull
    @Size(min = 10, message = "Please enter a valid address")
    private String address;


    @NotNull
    @Size(min = 8, message = "The password must have 8 characters or more")
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}" , 
            message = "Your password must have one upper, one lower, one digit and one special character")
    private String password;

    
    
    private String userType;
    

    /**
     * Creates a new instance of SignupController
     */
    public SignupController() {
    }

    
    
    
    
    
    /*
    * This method calls the ejb to add new user to the database. The values are
    obtaied through JSF UI components.
    * @Param : 
    * @Return : 
    */
    public void addNewUser() {
    
        userType = "2";
     boolean userCreation =  userDAOImpl.createUser(firstName, lastName, email, password, phoneNumber, address, userType);
    
     
     if (userCreation){
     
         String url = "https://localhost:11488/aussietravels-web/faces/public/index.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     
     }
     else {
     
         FacesContext.getCurrentInstance().addMessage("address", new FacesMessage(LOGIN_ERROR,LOGIN_ERROR));
    
     
     }
     
    
    }
    
    
    // getters and setters
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

  
    
    
    
    
    
    
}
