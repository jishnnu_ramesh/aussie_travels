/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 */
@Named(value = "navigation")
@RequestScoped
public class Navigation {

    /**
     * Creates a new instance of Navigation
     */
    public Navigation() {
    }
    
    public String shownewpage() {
    
        return "test";
    }
    
    
     /*
    * This method redirects users to the index page
    * @Param : 
    * @Return : 
    */
    public void showSearch() {
        
        String url = "https://localhost:11488/aussietravels-web/faces/index.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
     /*
    * This method invalidates a user session and logs the user out
    * @Param : 
    * @Return : 
    */
    public void logout() {
    
    
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        String url = "https://localhost:11488/aussietravels-web/faces/index.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    
    
     /*
    * This method redirects the user to the registration screen
    * @Param : 
    * @Return : 
    */
    public void register() {
    
    
         FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        String url = "https://localhost:11488/aussietravels-web/faces/register.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    
    
    
    
    
    
    
}
