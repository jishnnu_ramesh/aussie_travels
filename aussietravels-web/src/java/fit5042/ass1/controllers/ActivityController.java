/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.activity.ActivityDAO;
import fit5042.ass1.booking.BookingDAO;
import fit5042.ass1.bookingtype.BookingTypeDAO;
import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import javax.validation.constraints.NotNull;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;



/**
 *
 * @author Jishnu
 */
@Named(value = "activityController")
@SessionScoped
public class ActivityController implements Serializable {

    /**
     * Creates a new instance of ActivityController
     */
    
    @NotNull
    private String date;

    private String activityID;
    
    private Activity activity;
    
    private HashMap<String,String> bookingTypeDropDown;

   
    private HashMap<String,String> numberOfPeople;
   
    @NotNull
    private String bookingName;

    private double totalPrice;
 
    @NotNull
    private String noOfPeopleText;
    
    private String bookingTypeSelection;

   
    
    @EJB
    private UserSearch userSearchImpl;
    
    
    @EJB
    private ActivityDAO activityDAOImpl;
    
    
    @EJB
    private BookingTypeDAO bookingTypeDAOImpl;
    
    
    @EJB
    private BookingDAO bookingDAOImpl;
    
    
    public ActivityController() {
    }
    
    
     @PostConstruct
    public void init() {
    
         if (activityID == null || activityID.length() == 0) {
        
            activityID = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("activity"); 
            
            //System.out.println(packageID);
            
        if (activityID == null || activityID.length() == 0) {
            
            redirectToError();

       }
          
 
    }
         
         populateData();
         
        
         
    }
    
    
    
    
    
     /*
    * This method populates the data into all the variable which are binded with JSF
    * @Param : 
    * @Return : 
    */
     public void populateData() {
    
         numberOfPeople = new HashMap<>();
         bookingTypeDropDown = new HashMap<>();
         numberOfPeople.put("1","1");
         numberOfPeople.put("2","2");
         numberOfPeople.put("3","3");
         numberOfPeople.put("4","4");
         numberOfPeople.put("5","5");
         numberOfPeople.put("6","6");
         numberOfPeople.put("7","7");
         numberOfPeople.put("8","8");
         numberOfPeople.put("9","9");
         
         noOfPeopleText = "1";
         date = "";
         
         activity = activityDAOImpl.findActivityById(activityID);
    
    
        bookingTypeDropDown =  bookingTypeDAOImpl.getAllBookingTypesFormWebService();
        
        
        totalPrice = activity.getActivityCost().doubleValue();
    
    
    
    
    
    }
    
    
     
     
      /*
    * This method handles the ajax requests when ever a value change occur 
     in the binded rows in the forum
    * @Param :  
    * @Return : 
    */
      public void valueChange() {
    
        totalPrice = Integer.parseInt(noOfPeopleText) * activity.getActivityCost().doubleValue();
    
    
    }
    
      
      
     /*
    * This method is responsible for redirecting the user to the correct payment page
    * @Param :  
    * @Return : 
    */
    public void pay() {
    
      if (bookingTypeSelection.equals("1")) {
        
       String url = "https://localhost:11488/aussietravels-web/faces/public/payment.xhtml?type=activity";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      else if (bookingTypeSelection.equals("2")) {
      
      String url = "https://localhost:11488/aussietravels-web/faces/public/voucherPayment.xhtml?type=activity";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
      
      }
    
    }
    
    
        
    
     /*
    * This method clears the variables in the managed bean
    * @Param :  
    * @Return : 
    */
    public long modelStateClear() {
        
    String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
    Users currentUser = userSearchImpl.getUserByEmail(email);
    
    
    Long id = bookingDAOImpl.saveActivityBooking(bookingName, activity, noOfPeopleText, bookingTypeSelection, totalPrice, currentUser, date);
     
    
    bookingName = "";
    noOfPeopleText = "";
    bookingTypeSelection = "";
    activity = null;
    totalPrice = 0; 
    date = "";
    
    return id;
  
    }
    
    
    
    
    
          
      /*
    * This method redirects user to an error page on
    * @Param :  
    * @Return : 
    */
    public void redirectToError() {
    
        String url = "https://localhost:11488/aussietravels-web/faces/public/error.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }   

    
    
    
    
    // Gettes and setters
    
    public String getActivityID() {
        return activityID;
    }

    public void setActivityID(String activityID) {
        this.activityID = activityID;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public HashMap<String, String> getBookingTypeDropDown() {
        return bookingTypeDropDown;
    }

    public void setBookingTypeDropDown(HashMap<String, String> bookingTypeDropDown) {
        this.bookingTypeDropDown = bookingTypeDropDown;
    }

    public HashMap<String, String> getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(HashMap<String, String> numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNoOfPeopleText() {
        return noOfPeopleText;
    }

    public void setNoOfPeopleText(String noOfPeopleText) {
        this.noOfPeopleText = noOfPeopleText;
    }

    public String getBookingTypeSelection() {
        return bookingTypeSelection;
    }

    public void setBookingTypeSelection(String bookingTypeSelection) {
        this.bookingTypeSelection = bookingTypeSelection;
    }
      
    
      public String getDate() {
        return date;
    }

    public void setDate(String Date) {
        this.date = Date;
    }

    
}
