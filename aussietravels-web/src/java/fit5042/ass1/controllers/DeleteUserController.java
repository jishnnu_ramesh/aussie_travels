/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserSearch;
import fit5042.ass1.users.UserDAO;
import java.io.IOException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 */
@Named(value = "deleteUserController")
@RequestScoped
public class DeleteUserController {

    private String pageTitle;
    private String userId;
    private Users user;
    private boolean resultAvailable;
    
    
    @EJB
    private UserSearch userSearchImpl;
    
    
    @EJB
    private UserDAO userDAOImpl;
    
    
    
    /**
     * Creates a new instance of DeleteUserController
     */
    public DeleteUserController() {
     
    }
    
    
    @PostConstruct
    public void init() {
    
    userId = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("userDelete");
    
    pageTitle  = "Delete User";
        
    delete();
    
    }
    
    
     /*
    * This method redirect admin to error page if any error occurs
    * @Param : 
    * @Return : 
    */
    public void redirectToError(String error){
    
    
             String url = "https://localhost:11488/aussietravels-web/admin/error.xhtml" ;
             try {
                 //FacesContext.getCurrentInstance().addMessage("delete-form:delete-error", new FacesMessage(error,error));
                  FacesContext.getCurrentInstance().getExternalContext().redirect(url);
             } catch (IOException ex1) {
                 Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, ex1);
             }

    }

    
    
    
     /*
    * This method calls the ejb to delete the user based on the selection
    * @Param : 
    * @Return :
    */
     public void delete() {
     
     
         try {
         
             
             String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
             Users id = userSearchImpl.getUserByEmail(email);
             
             
             // If admin try to delete himself he will get an error
             if (id == null || id.getUserId().toString().equals(userId) ) {
             
              Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, email);
              redirectToError("Something is wring with the requesst");
      
             }
             else {
             
                 
              Boolean delete = userDAOImpl.deleteUser(userId);
              
              if (delete) {
              
                  redirectToSucess();
              
              }
              else {
              
                  redirectToError("Something is wrong with the request");
              
              
              }
              
          
             
             }

         }
         
         catch (Exception ex) {
         
             redirectToError("something is wrong with the request");
         
         }
         
         
     
     }
    
    
     
     /*
    * This method redirects users to a sucess page
    * @Param : 
    * @Return : 
    */
   private void redirectToSucess() {
       
        String url = "https://localhost:11488/aussietravels-web/admin/index.xhtml";
             try {
                 FacesContext.getCurrentInstance().getExternalContext().redirect(url);
             } catch (IOException ex1) {
                 Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, ex1);
             }
       
       
   }

     
     
     
     
     //Getters and setters
    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public boolean isResultAvailable() {
        return resultAvailable;
    }

    public void setResultAvailable(boolean resultAvailable) {
        this.resultAvailable = resultAvailable;
    }

   
    
   
    
     
     
     
     
     
     
     
     
    
    
}
