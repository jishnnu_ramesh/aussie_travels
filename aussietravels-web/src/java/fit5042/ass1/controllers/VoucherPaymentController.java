/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.payment.PaymentDAO;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Jishnu
 */
@Named(value = "voucherPaymentController")
@SessionScoped
public class VoucherPaymentController implements Serializable {

    
    @NotNull
    @Pattern(regexp = "AUZZIEFREE", message = "Your have enetered an invalid voucher code")
    private String voucherCode;
    
    
    private String type;
    
    private double price;

    
    @EJB
    private PaymentDAO paymentDAOImpl;
    
    //https://stackoverflow.com/questions/12356990/how-to-access-property-of-one-managed-bean-in-another-managed-bean
    @Inject
    private ResortController resortController;
    
    
    @Inject
    private PackageController packageController;
    
    @Inject
    private ActivityController activityController; 
    
    
    
    /**
     * Creates a new instance of voucherPaymentController
     */
    public VoucherPaymentController() {
        type = "";
        
    }
    
    
    
    
     public void init() {
    
    
       type = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("type"); 

       
       if (type.equals("packages")) {
       
           price = packageController.getTotalPrice();
       
       
       }
       else if(type.equals("resorts")) {
       
           price = resortController.getTotalPrice();
       
       }
       else if (type.equals("activity")) {
       
       
            price = activityController.getTotalPrice();
       }
        

    }
     
     
     
      /*
    * This method checks if the voucher code is valid or not
    * @Param : 
    * @Return : 
    */
     public void activateVoucherCode(){
         
         
         if (voucherCode.equals("AUZZIEFREE")){
         
             price = 0.0;
         
         }
         
         
     }
     
     
     
      /*
    * This method takes care of the payment and booking of the travel experience
    * @Param : 
    * @Return : 
    */
     public void pay() {
         
         Long ans = paymentDAOImpl.voucherPayment(voucherCode);
       
       if (type.equals("resorts")) {
       
           resortController.setTotalPrice(0.0);
           long id = resortController.modelStateClear();
           modelStateClear();  
           redirectToBooking(id,1);
          
       }
       else if (type.equals("packages")) {
       
           packageController.setTotalPrice(0.0);
           long id = packageController.modelStateClear();
           modelStateClear();  
           redirectToBooking(id,2);
       }
       else if (type.equals("activity")) {
       
           activityController.setTotalPrice(0.0);
           long id = activityController.modelStateClear();
           modelStateClear();  
           redirectToBooking(id,3);
       
       }
       
            
     
     
     }
     
     
     
      /*
    * This method redirects user to the booking confirmation page
    * @Param : 
    * @Return : 
    */
     public void redirectToBooking(long id, int num) {
    
    if (id == 0L) {
    
    redirectToIndex();
    
    }
    else if (num == 1) {
    
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/resortbookings.xhtml?resortBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    else if (num == 2) {
    
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/packagebookings.xhtml?packageBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    else if (num == 3) {
    
        String url = "https://localhost:11488/aussietravels-web/faces/public/activitybookings.xhtml?activityBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    }
    
    
    
    }
    
    
     
     
     
     
     
     
      /*
    * This method clears all the variables of the bean which are binded to the
     JSF page
    * @Param : 
    * @Return : 
    */
     public void modelStateClear() {
     
         price = 0.0;
         voucherCode = "";
   
     }
     
     
     
     
      /*
    * This method redirects user to the index page
    * @Param : 
    * @Return : 
    */
      public void redirectToIndex() {
    
    
         String url = "https://localhost:11488/aussietravels-web/faces/index.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

      
      
      // Getters and setters
    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ResortController getResortController() {
        return resortController;
    }

    public void setResortController(ResortController resortController) {
        this.resortController = resortController;
    }

    public PackageController getPackageController() {
        return packageController;
    }

    public void setPackageController(PackageController packageController) {
        this.packageController = packageController;
    }

    public ActivityController getActivityController() {
        return activityController;
    }

    public void setActivityController(ActivityController activityController) {
        this.activityController = activityController;
    }
     
     
      
      
      
    
}
