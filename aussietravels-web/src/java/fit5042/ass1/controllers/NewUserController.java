/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.users.UserDAO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Jishnu
 */
@Named(value = "newUserController")
@SessionScoped
public class NewUserController implements Serializable {

    @EJB
    private UserDAO userDAOImpl;
    
    
    private Map<String,String> userType;
    
    @NotNull
    @Size(min = 2, max = 30, message = " First Name should be at least 2 letters long")
    @Pattern(regexp = "[A-Z][a-z]+", message = "The first name should start with capital letters and should have only alphabets " )
    private String firstName;
    
    @NotNull
    @Size(min = 2, max = 30,  message = " Last Name should be at least 2 letters long")
    @Pattern(regexp = "[A-Z][a-z]+", message = "The last name should start with capital letters and should have only alphabets " )
    private String lastName;
    
    @NotNull
    @Size(min = 5, max = 30,  message = "invalid email")
    @Email
    private String email;
   
    @Size(min = 8 , max = 10, message = "Invalid Phone Number")
    @Pattern(regexp = "0[0-9]{9}|9[0-9]{7}", message = "Should be a valid mobile or land phone number ")
     private String phoneNumber;
    
    
    @NotNull
    @Size(min = 10, message = "Please enter a valid address")
    private String address;

    @NotNull
    @Pattern(regexp = "1|2", message = "Please specify the correct user type")
    private String userTypeText;
    
    
    @NotNull
    @Size(min = 8, message = "The password must have 8 characters or more")
    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}" , 
            message = "Your password must have one upper, one lower, one digit and one special character")
    private String password;

   
  
    
    
    /**
     * Creates a new instance of NewUserController
     */
    public NewUserController() {
        
        userType = new HashMap<>();
        userType.put("Admin", "1");
        userType.put("Public", "2");
        
    }
    
    
    
    public void init() {
    
        userType = new HashMap<>();
        userType.put("Admin", "1");
        userType.put("Public", "2");
        firstName = "";
        lastName = "";
        phoneNumber = "";
        address = "";
        email = "";
        password = "";
        
        
    
    }
    
    
    
     /*
    * This method calls the ejb method to create anew user.
    * @Param : 
    * @Return :
    */
    public void addUser() {
        
        System.out.println("yesy");
        Boolean result = userDAOImpl.createUser(firstName, lastName, email, password, phoneNumber, address, userTypeText);
        
        if (result) {
        
            init();
        }
        
        
        
    }
    
    
    
    
    // getters and setters
    
      public Map<String, String> getUserType() {
        return userType;
    }

    public void setUserType(Map<String, String> userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserTypeText() {
        return userTypeText;
    }

    public void setUserTypeText(String userTypeText) {
        this.userTypeText = userTypeText;
    }
    
    
     public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    
    
}
