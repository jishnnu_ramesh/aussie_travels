/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserSearch;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Jishnu
 */
@Named(value = "adminController")
@RequestScoped
public class AdminController implements Serializable {

    @EJB
    private UserSearch userSearch;
    
    
    private String pageTitle;
    private Map<String, String> firstDropDown;
    private Map<String, String> secondDropDown;
    private String firstDropDownValue;
    private String secondDropDownValue;
    private String firstCriteriaText;
    private String secondCriteriaText;
    private boolean searchResultAvailable;
    private boolean firstTimeLoad;
    private ArrayList<Users> result;
    private final String EMAIL_ERROR = "Invalid Email : Please provide a valid email";
    private final String NAME_ERROR = "Invalid Name : Name should start with caiptal letters and should only contain alphabets";
    private final String TYPE_ERROR = "Invalid Type : Type should be either Public/ Admin";
    private final String ID_ERROR = "Invalid ID : Please provide a valid Id";
    
    
    /**
     * Creates a new instance of AdminController
     */
    public AdminController() {
        
        firstDropDown = new HashMap<String,String>();
        secondDropDown = new HashMap<String,String>();
        firstTimeLoad = true;
        
    }
    
    @PostConstruct
    public void init() {
    
        pageTitle = "ADMIN";
        this.firstDropDown.put("UserID","1");
        this.firstDropDown.put("Last Name","2");
        this.firstDropDown.put("First Name","3");
        this.firstDropDown.put("Email","4");
        this.firstDropDown.put("Type","5");
        this.secondDropDown.put("UserID","1");
        this.secondDropDown.put("Last Name","2");
        this.secondDropDown.put("First Name","3");
        this.secondDropDown.put("Email","4");
        
        
    }
    
     /*
    * This method populates the drop down based oon the user selction 
    for first drop down via ajax requests
    * @Param : int option
    * @Return : Map
    */
    public Map<String,String> populateSecondDropDown( int option) {
        
        Map<String,String> newMap = new HashMap<String,String>();
        
        if (option != 1) {
            newMap.put("UserID","1");
        }
        if (option != 2) {
            newMap.put("Last Name","2");
        }
        if (option != 3) {
            newMap.put("First Name","3");
        }
        if (option != 4) {
            newMap.put("Email","4");
        }
        if (option != 5) {
            newMap.put("Type","5");
        }
        
        
 
        return newMap;
        
    }
    
    
    
    
     /*
    * This method populates the data based on change in the drop down selcetion
    * @Param : 
    * @Return : 
    */
    public void changeDropDown() {
        
        
        try {
            
            int option = Integer.parseInt(firstDropDownValue);
            secondDropDown = populateSecondDropDown(option);
        }
        catch (Exception ex) {
        
            System.out.println("Some Issue Occured");
        
        }
        
        
    }
    
    
    
     /*
    * This method calls the ejb for searching the user based on the selection
    criteria
    * @Param : 
    * @Return : 
    */
    public void search() {
        
       result = new ArrayList<Users>();
       searchResultAvailable = false;
       firstTimeLoad = false;
       boolean fValid =  validateFirstField();
       boolean sValid = validateSecondField();
       
       try {
       
       if (fValid && sValid) {
       
           if (firstDropDownValue.equals("1") && secondDropDownValue.equals("2") ) {
           
               result = userSearch.getUsersByIdAndLastName(firstCriteriaText, secondCriteriaText);
               fValid = false;
           }
           
           else if (firstDropDownValue.equals("1") && secondDropDownValue.equals("3")) {
           
                result = userSearch.getUsersByIdAndFirstName(firstCriteriaText, secondCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("1") && secondDropDownValue.equals("4")) {
           
                result = userSearch.getUsersByIdAndEmail(firstCriteriaText, secondCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("1") && secondDropDownValue.equals("5")) {
           
                result = userSearch.getUsersByIdAndType(firstCriteriaText, secondCriteriaText);
           
           }
           else if (firstDropDownValue.equals("2") && secondDropDownValue.equals("1")) {
           
                result = userSearch.getUsersByIdAndLastName(secondCriteriaText, firstCriteriaText);
           
           }
           else if (firstDropDownValue.equals("2") && secondDropDownValue.equals("3")) {
           
                result = userSearch.getUsersByLastNameAndFirstName(firstCriteriaText, secondCriteriaText);
           
           }
           else if (firstDropDownValue.equals("2") && secondDropDownValue.equals("4")) {
           
                result = userSearch.getUsersByLastNameAndEmail(firstCriteriaText, secondCriteriaText);
           
           }
           else if (firstDropDownValue.equals("2") && secondDropDownValue.equals("5")) {
           
                result = userSearch.getUsersByLastNameAndType(firstCriteriaText, secondCriteriaText);
           
           }
           else if (firstDropDownValue.equals("3") && secondDropDownValue.equals("1")) {
           
                result = userSearch.getUsersByIdAndFirstName(secondCriteriaText, firstCriteriaText);
           
           }
           else if (firstDropDownValue.equals("3") && secondDropDownValue.equals("2")) {
           
                result = userSearch.getUsersByLastNameAndFirstName(secondCriteriaText, firstCriteriaText);
           
           }
            else if (firstDropDownValue.equals("3") && secondDropDownValue.equals("4")) {
           
                result = userSearch.getUsersByFirstNameAndEmail(firstCriteriaText, secondCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("3") && secondDropDownValue.equals("5")) {
           
                result = userSearch.getUsersByFirstNameAndType(firstCriteriaText, secondCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("4") && secondDropDownValue.equals("1")) {
           
                result = userSearch.getUsersByIdAndEmail(secondCriteriaText, firstCriteriaText);
           
           }
           
       
           else if (firstDropDownValue.equals("4") && secondDropDownValue.equals("2")) {
           
                result = userSearch.getUsersByLastNameAndEmail(secondCriteriaText, firstCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("4") && secondDropDownValue.equals("3")) {
           
                result = userSearch.getUsersByFirstNameAndEmail(secondCriteriaText, firstCriteriaText);
           
           }
           
           else if (firstDropDownValue.equals("4") && secondDropDownValue.equals("5")) {
           
                result = userSearch.getUsersByEmailAndType(firstCriteriaText, secondCriteriaText);
           
           }
           else if (firstDropDownValue.equals("5") && secondDropDownValue.equals("1")) {
           
                result = userSearch.getUsersByIdAndType(secondCriteriaText, firstCriteriaText);
           
           }
           else if (firstDropDownValue.equals("5") && secondDropDownValue.equals("2")) {
           
                result = userSearch.getUsersByLastNameAndType(secondCriteriaText, firstCriteriaText);
           
           }
            else if (firstDropDownValue.equals("5") && secondDropDownValue.equals("3")) {
           
                result = userSearch.getUsersByFirstNameAndType(secondCriteriaText, firstCriteriaText);
           
           }
            else if (firstDropDownValue.equals("5") && secondDropDownValue.equals("4")) {
           
                result = userSearch.getUsersByEmailAndType(secondCriteriaText, firstCriteriaText);
           
           }
           
           
       }
       }
       
       
           catch (ConstraintViolationException e) {
                   

                e.getConstraintViolations().forEach(err->System.out.println(err.toString()));
    
       
       }
       
       
       if (!result.isEmpty()) {
       
           searchResultAvailable = true;
       
       }
       
       
          
    }
    
    
    
    
    
     /*
    * This method validates if the first search field is having a valid input
    * @Param : 
    * @Return : A boolean
    */
    private Boolean validateFirstField() {
    
        boolean fValid = false;
        
       if (firstDropDownValue.equals("1"))
        {
            fValid = validateId(firstCriteriaText);
            if (!fValid) {
            
                
                FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-1", new FacesMessage(ID_ERROR,ID_ERROR));
     
                
            }
            
        }
        else if (firstDropDownValue.equals("2") | firstDropDownValue.equals("3"))
        {
        
            fValid = validateName(firstCriteriaText);
            
             if (!fValid) {
            
                
                FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-1", new FacesMessage(NAME_ERROR,NAME_ERROR));
    
                
            }
            
        
        }
        else if (firstDropDownValue.equals("4")) {
        
            fValid = validateEmail(firstCriteriaText);
            
             if (!fValid) {
            
                
               FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-1", new FacesMessage(EMAIL_ERROR,EMAIL_ERROR));
       
                
            }
            
        
        }
        else if (firstDropDownValue.equals("5")) {
            
            fValid = validateType(firstCriteriaText);
            
            if (!fValid) {
            
                
               FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-1", new FacesMessage(TYPE_ERROR,TYPE_ERROR));
          
            }

        }
    
        return fValid;
    
    }
    
    
    
    
     /*
    * This method validates if the second search field is having a valid input
    * @Param : 
    * @Return : A boolean
    */
      private Boolean validateSecondField() {
    
        boolean sValid = false;
        
       if (secondDropDownValue.equals("1"))
        {
            sValid = validateId(secondCriteriaText);
            if (!sValid) {
            
                
                FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-2", new FacesMessage(ID_ERROR, ID_ERROR));
     
                
            }
            
        }
        else if (secondDropDownValue.equals("2") | secondDropDownValue.equals("3"))
        {
        
            sValid = validateName(secondCriteriaText);
            
             if (!sValid) {
            
                
                FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-2", new FacesMessage(NAME_ERROR,NAME_ERROR));
    
                
            }
            
        
        }
        else if (secondDropDownValue.equals("4")) {
        
            sValid = validateEmail(secondCriteriaText);
            
             if (!sValid) {
            
                
               FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-2", new FacesMessage(EMAIL_ERROR, EMAIL_ERROR));
       
                
            }
            
        
        }
        else if (secondDropDownValue.equals("5")) {
            
            sValid = validateType(secondCriteriaText);
            
            if (!sValid) {
            
                
               FacesContext.getCurrentInstance().addMessage("user-form:input-search-text-2", new FacesMessage(TYPE_ERROR,TYPE_ERROR));
          
            }

        }
    
        return sValid;
    
    }
    
    
    
    
    
     /*
    * This method validates the ID provided by the admin for user search and
      returns true if id is valid else false
    * @Param : 
    * @Return : A boolean
    */
    private boolean validateId(String test) {

        try{
            
            int Id = Integer.parseInt(test);
            return true;
        
        }
        catch (Exception ex) {
            
             return false;
        
        }

    }
    
    
    
    
     /*
    * This method validates the name provided by the admin for user search and
      returns true if id is valid else false
    * @Param : 
    * @Return : A boolean
    */
    private boolean validateName(String test) {
      
        if (Pattern.matches("[A-Z][a-z]+", test)) {
            return true;
        }
        else {
            return false;
        }
       
    }
    
    
    
     /*
    * This method validates the email provided by the admin for user search and
      returns true if id is valid else false
    * @Param : 
    * @Return : A boolean
    */
    private boolean validateEmail(String test) {
    
         boolean result = true;
            try {
            InternetAddress emailAddr = new InternetAddress(test);
            emailAddr.validate();
            result = true;
            } 
            catch (AddressException ex) {
              result = false;
                
            }
            return result;
        
    }
    
    
    
     /*
    * This method validates the user type provided by the admin for user search and
      returns true if id is valid else false
    * @Param : 
    * @Return : A boolean
    */
    private boolean validateType(String test) {
    
       if (test.equalsIgnoreCase("public") | test.equalsIgnoreCase("admin") ) {
       
           return true;
       
       }
         
       else{
       
           return false;
       
       }
         
    }

    
    
    
    
    public boolean isFirstTimeLoad() {
        return firstTimeLoad;
    }

    public void setFirstTimeLoad(boolean firstTimeLoad) {
        this.firstTimeLoad = firstTimeLoad;
    }
    
    
    
    
    
    
    public boolean isSearchResultAvailable() {
        return searchResultAvailable;
    }

    public void setSearchResultAvailable(boolean searchResultAvailable) {
        this.searchResultAvailable = searchResultAvailable;
    }
    
    
    
      public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        
        this.pageTitle = pageTitle;
        
        
    }
    
 

    public Map<String, String> getFirstDropDown() {
        return firstDropDown;
    }

    public void setFirstDropDown(Map<String, String> firstDropDown) {
        this.firstDropDown = firstDropDown;
    }

    public Map<String, String> getSecondDropDown() {
        return secondDropDown;
    }

    public void setSecondDropDown(Map<String, String> secondDropDown) {
        this.secondDropDown = secondDropDown;
    }
    
     public String getFirstDropDownValue() {
        return firstDropDownValue;
    }

    public void setFirstDropDownValue(String firstDropDownValue) {
        this.firstDropDownValue = firstDropDownValue;
    }

    public String getSecondDropDownValue() {
        return secondDropDownValue;
    }

    public void setSecondDropDownValue(String secondDropDownValue) {
        this.secondDropDownValue = secondDropDownValue;
    }
    
    
        public String getFirstCriteriaText() {
        return firstCriteriaText;
    }

    public void setFirstCriteriaText(String firstCriteriaText) {
        this.firstCriteriaText = firstCriteriaText;
    }

    public String getSecondCriteriaText() {
        return secondCriteriaText;
    }

    public void setSecondCriteriaText(String secondCriteriaText) {
        this.secondCriteriaText = secondCriteriaText;
    }
    
    
    public ArrayList<Users> getResult() {
        return result;
    }

    public void setResult(ArrayList<Users> result) {
        this.result = result;
    }

   
    
    
}
