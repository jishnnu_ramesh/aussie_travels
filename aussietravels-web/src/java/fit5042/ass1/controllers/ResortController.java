/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.booking.BookingDAO;
import fit5042.ass1.bookingtype.BookingTypeDAO;
import fit5042.ass1.entities.BookingType;
import fit5042.ass1.entities.Resorts;
import fit5042.ass1.entities.Rooms;
import fit5042.ass1.entities.Users;
import fit5042.ass1.resort.ResortDAO;
import fit5042.ass1.roomtype.RoomTypeDAO;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.json.JsonObject;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jishnu
 */
@Named(value = "resortController")
@SessionScoped
public class ResortController implements Serializable {

    /**
     * Creates a new instance of ResortController
     */
    
    @NotNull
    private String date;
    
    private String resortID;

    private Resorts resort;

    private ArrayList<Rooms> rooms;
    
    private HashMap<String,String> roomTypeDropDown;

    private HashMap<String,String> numberOfPeople;
    
    private HashMap<String,String> AirportPickupDropDown;

    private String bookingTypeSelection;
 
    private HashMap<String,String> bookingTypeDropDown;
    
    private double airportPickUpCost;
    

    private String roomTypeSelection;
      
    private double totalPrice;
    
    
    
    
    @NotNull
    private String noOfPeopleText;
    
    private String airportPickup;
    
    private int numberOfRooms;
    
    @NotNull
    private String bookingName;

    
    private Rooms selectedRoom;
    
   
   
    
   
    
    private BookingType bookingType;

   

    
    private double roomCost;

   
   
    @EJB
    private UserSearch userSearchImpl;
    
    @EJB
    private ResortDAO resortDAOImpl;
    
    
    @EJB
    private RoomTypeDAO roomTypeDAOImpl;
    
    @EJB
    private BookingTypeDAO bookingTypeDAOImpl;
    
    
    @EJB
    private BookingDAO bookingDAOImpl;
    
    
    
    public ResortController() {
    }
    
    
    @PostConstruct
    public void init() {
        
       
        bookingTypeDropDown = new HashMap<String,String>();
        roomTypeDropDown = new HashMap<String,String>();
        
        if (resortID == null || resortID.length() == 0) {
        
            resortID = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("bookResorts"); 
            
            
        if (resortID == null || resortID.length() == 0) {
            
            redirectToError();

       }
            
            
     }
        
        
      populateItems();
        
   
    }
    

    
     /*
    * This method populate the variables of the bean which are binded with
    the JSF pages
    * @Param : 
    * @Return : 
    */
     private void populateItems() {
     
         numberOfPeople = new HashMap<>();
         AirportPickupDropDown = new HashMap<>();
         
         
         AirportPickupDropDown.put("No","0");
         AirportPickupDropDown.put("Yes", "1");
         
         numberOfPeople.put("1","1");
         numberOfPeople.put("2","2");
         numberOfPeople.put("3","3");
         numberOfPeople.put("4","4");
         numberOfPeople.put("5","5");
         numberOfPeople.put("6","6");
         numberOfPeople.put("7","7");
         numberOfPeople.put("8","8");
         numberOfPeople.put("9","9");

           
         try{
         
             int id = Integer.parseInt(resortID);
             resort = resortDAOImpl.getResortById(id);
             rooms = roomTypeDAOImpl.getAllRooms();
             
             
             for (Rooms r : rooms) {
             
                 roomTypeDropDown.put(r.getRoomName(), String.valueOf(r.getRoomId()));
             
             
             }
         
        noOfPeopleText = "1";
        numberOfRooms = 1;     
        totalPrice = 0; 
        airportPickup = "0";
        
        bookingTypeDropDown =  bookingTypeDAOImpl.getAllBookingTypesFormWebService();
         date = "";
        
         }
         catch (Exception ex)
         {
             
          redirectToError();
             
         }
         
     
     
     
     
     }
    
    
    
    
     /*
    * This method redirects user to an error page if any error occurs
    * @Param : 
    * @Return :
    */
    public void redirectToError() {
    
        String url = "https://localhost:11488/aussietravels-web/faces/public/error.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
  
    
     /*
    * This method changes the value of the total price for the booking based on
    the selection changes uer perform in the booking page. This method is activated 
    by Ajax call
    * @Param : 
    * @Return : 
    */
    public void valueChange() {
   
        
        selectedRoom = roomTypeDAOImpl.getRoomById(roomTypeSelection);
       
       // Room cost
       if (selectedRoom != null) {
       
           roomCost = selectedRoom.getRoomCost().doubleValue();
       }
       
       try{
           
            // Airport cost
            
            if (airportPickup.equals("1")) {
            
            
                airportPickUpCost = 50.00;
            
            }
            else {
            
                airportPickUpCost = 0.0;
            
            }
           
           
           
            int people = Integer.parseInt(noOfPeopleText);
            double number = (double) people / (double) selectedRoom.getRoomOccupancy();
            numberOfRooms = (int) Math.ceil(number);
            if (numberOfRooms == 0) {
            
                numberOfRooms = 1;
            }
            
     
            totalPrice = (roomCost * numberOfRooms) + airportPickUpCost;
            
       }
       catch (Exception ex) {
       
       
           totalPrice = (roomCost * numberOfRooms) + airportPickUpCost;
       }
    
    
    }
    
    
    
    
     /*
    * This method redirects user to the payment gateway based on the type of 
    payment selected
    * @Param : 
    * @Return : 
    */
    public void pay() {
    
     
        if (bookingTypeSelection.equals("1")) {
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/payment.xhtml?type=resorts";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      else if (bookingTypeSelection.equals("2")) {
      
      String url = "https://localhost:11488/aussietravels-web/faces/public/voucherPayment.xhtml?type=resorts";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
      
      }
    
    
    }
    
    
    
    
     /*
    * This method clears the varaibles on the bean after succesfull booking
    * @Param : 
    * @Return : 
    */
    public long modelStateClear() {
        
        
        String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
        Users currentUser = userSearchImpl.getUserByEmail(email);
    
        Long bookingId =  bookingDAOImpl.saveResortBooking(bookingName, resort, selectedRoom, noOfPeopleText, numberOfRooms, airportPickup, bookingTypeSelection, totalPrice, currentUser, date);
        
        
        
      resortID = "";

      resort = null;

     rooms  = new ArrayList<>();
    
    roomTypeDropDown = new HashMap<>();

     numberOfPeople = new HashMap<>();
    
    AirportPickupDropDown = new HashMap<>();

   bookingTypeSelection = "";
 
     bookingTypeDropDown =  new HashMap<>();;
    
    airportPickUpCost = 0.0;
    

    roomTypeSelection = "";
      
     totalPrice = 0;
   
     noOfPeopleText = "";
    
     airportPickup = "";
  
      numberOfRooms = 0;
    
       bookingName = "";

      roomCost = 0;

       date = "";
       
       
       return bookingId;
       
    
    }
    
    
    
    
    
    
    
    
    // Getters and setters
     public String getResortID() {
        return resortID;
    }

    public void setResortID(String resortID) {
        this.resortID = resortID;
    }

  
    public ArrayList<Rooms> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Rooms> rooms) {
        this.rooms = rooms;
    }

   

    public String getRoomTypeSelection() {
        return roomTypeSelection;
    }

    public void setRoomTypeSelection(String roomTypeSelection) {
        this.roomTypeSelection = roomTypeSelection;
    }
   
    public Resorts getResort() {
        return resort;
    }

    public void setResort(Resorts resort) {
        this.resort = resort;
    }
   
    
    public HashMap<String, String> getRoomTypeDropDown() {
        return roomTypeDropDown;
    }

    public void setRoomTypeDropDown(HashMap<String, String> roomTypeDropDown) {
        this.roomTypeDropDown = roomTypeDropDown;
    }
    
    
      public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNoOfPeople() {
        return noOfPeopleText;
    }

    public void setNoOfPeople(String noOfPeople) {
        this.noOfPeopleText = noOfPeople;
    }

    public String getAirportPickup() {
        return airportPickup;
    }

    public void setAirportPickup(String airportPickup) {
        this.airportPickup = airportPickup;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public ResortDAO getResortDAOImpl() {
        return resortDAOImpl;
    }

    public void setResortDAOImpl(ResortDAO resortDAOImpl) {
        this.resortDAOImpl = resortDAOImpl;
    }
    
    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }
    
    
     public String getBookingTypeSelection() {
        return bookingTypeSelection;
    }

    public void setBookingTypeSelection(String bookingTypeSelection) {
        this.bookingTypeSelection = bookingTypeSelection;
    }
    
    
     public HashMap<String, String> getBookingTypeDropDown() {
        return bookingTypeDropDown;
    }

    public void setBookingTypeDropDown(HashMap<String, String> bookingTypeDropDown) {
        this.bookingTypeDropDown = bookingTypeDropDown;
    }
    
    
    
    public HashMap<String, String> getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(HashMap<String, String> numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public String getNoOfPeopleText() {
        return noOfPeopleText;
    }

    public void setNoOfPeopleText(String noOfPeopleText) {
        this.noOfPeopleText = noOfPeopleText;
    }

    public double getRoomCost() {
        return roomCost;
    }

    public void setRoomCost(double roomCost) {
        this.roomCost = roomCost;
    }
    
    
     public HashMap<String, String> getAirportPickupDropDown() {
        return AirportPickupDropDown;
    }

    public void setAirportPickupDropDown(HashMap<String, String> AirportPickupDropDown) {
        this.AirportPickupDropDown = AirportPickupDropDown;
    }
    
     public double getAirportPickUpCost() {
        return airportPickUpCost;
    }

    public void setAirportPickUpCost(double airportPickUpCost) {
        this.airportPickUpCost = airportPickUpCost;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
   

    
    
    
    
}
