/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Jishnu
 */
@Named(value = "indexController")
@RequestScoped
public class IndexController {

     private String pageTitle;

    public IndexController() {
    // Set the page title 
        pageTitle = "Aussie Holidays";
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    
}
