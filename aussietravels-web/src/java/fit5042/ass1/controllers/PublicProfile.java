/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserDAO;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 */
@Named(value = "publicUserDetails")
@SessionScoped
public class PublicProfile implements Serializable {

    private Users user;
    private boolean resultAvailable;
    
    
    @EJB
    private UserSearch userSearchImpl;
    
    
    @EJB
    private UserDAO userDAOImpl;
    
    
    
    /**
     * Creates a new instance of publicUserDetails
     */
    public PublicProfile() {
    }
    
    
    
    @PostConstruct
    public void init() {
    
    String email = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
             
             user = userSearchImpl.getUserByEmail(email);

             if (user == null ) {
             
              Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, email);
              redirectToError("Something is wring with the requesst");
      
             }
    }
    
    
    
    
     /*
    * This method redirects the user into a error page
    * @Param : String error
    * @Return : 
    */
    public void redirectToError(String error){
    
    
             String url = "https://localhost:11488/aussietravels-web/public/error.xhtml" ;
             try {
                 //FacesContext.getCurrentInstance().addMessage("delete-form:delete-error", new FacesMessage(error,error));
                  FacesContext.getCurrentInstance().getExternalContext().redirect(url);
             } catch (IOException ex1) {
                 Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, ex1);
             }

    }

    
    
    // Getters and setters
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public boolean isResultAvailable() {
        return resultAvailable;
    }

    public void setResultAvailable(boolean resultAvailable) {
        this.resultAvailable = resultAvailable;
    }
    
    
    
    
    
    
    
    
}
