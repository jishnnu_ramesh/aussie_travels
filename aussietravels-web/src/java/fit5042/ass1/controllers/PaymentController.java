/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.payment.PaymentDAO;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Jishnu
 */
@Named(value = "paymentController")
@SessionScoped
public class PaymentController implements Serializable {

  
    @EJB
    private PaymentDAO paymentDAOImpl;
    
    @Inject
    private ResortController resortController;
    
    
    @Inject
    private PackageController packageController;
    
    @Inject
    private ActivityController activityController;
    
    
    
    
    @NotNull
    private double price;
    
    @NotNull
    @Pattern(regexp = "^(?:(?<visa>4[0-9]{12}(?:[0-9]{3})?)|\n" +
"		(?<mastercard>5[1-5][0-9]{14})|\n" +
"		(?<discover>6(?:011|5[0-9]{2})[0-9]{12})|\n" +
"		(?<amex>3[47][0-9]{13})|\n" +
"		(?<diners>3(?:0[0-5]|[68][0-9])?[0-9]{11})|\n" +
"		(?<jcb>(?:2131|1800|35[0-9]{3})[0-9]{11}))$", message = "Invalid credit card number")
    private String creditCard;
    
    
    @NotNull
    @Pattern(regexp = "0?[1-9]|1[012]", message = "Invalid month")
    private String month;
    
    
    @NotNull
    private String year;
    
    
    @NotNull
    private String name;
    
    
    
    @NotNull
    private String type;

   
   
    
    
    /**
     * Creates a new instance of PaymentController
     */
    public PaymentController() {
         
        
    }
    
    
    
    
    
    
    public void init() {
    
    
       type = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("type"); 

       
       if (type.equals("packages")) {
       
           price = packageController.getTotalPrice();
       
       
       }
       else if(type.equals("resorts")) {
       
           price = resortController.getTotalPrice();
       
       }
       else if (type.equals("activity")) {
       
       
            price = activityController.getTotalPrice();
       }
        
        
       
    
    }
    
    
    
    
     /*
    * This method calls the EJB to process the payments and take care of 
    clearing the session beans after sucessful booking
    * @Param : 
    * @Return : 
    */
    public void pay () {
    
        Boolean valid = validateYearAndMonth();
        
      if (valid) {
    
       Long ans =  paymentDAOImpl.creditCardPaymentImpl(name,price);
       
       if (type.equals("resorts")) {
       
           Long id = resortController.modelStateClear();
           modelStateClear();
          redirectToBooking(id,1);
       }
       else if (type.equals("packages")) {
       
           Long id =  packageController.modelStateClear();
            modelStateClear();
          redirectToBooking(id,2);
       }
       else if (type.equals("activity")) {
       
           Long id = activityController.modelStateClear();
           modelStateClear();
          redirectToBooking(id,3);
       
       }
       
      }
      
    }
    
    
    
    
     /*
    * This method redirect the user to the booking confirmation screen on
    successfull booking
    * @Param : Long id, int num
    * @Return :
    */
    public void redirectToBooking(long id, int num) {
    
    if (id == 0L) {
    
    redirectToIndex();
    
    }
    else if (num == 1) {
    
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/resortbookings.xhtml?resortBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    else if (num == 2) {
    
        
        String url = "https://localhost:11488/aussietravels-web/faces/public/packagebookings.xhtml?packageBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    else if (num == 3) {
    
        String url = "https://localhost:11488/aussietravels-web/faces/public/activitybookings.xhtml?activityBookingId=" + String.valueOf(id);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    }
    
    
    
    }
    
    
    
    
     /*
    * This method clears the variables in the payment controller
    * @Param : 
    * @Return : 
    */
    public void modelStateClear(){
    
        price = 0.0;
    
       creditCard = "";
       month = "";
   
       year = "";
        name = "";
   
    }
    
    
    
     /*
    * This method redirects users to index page
    * @Param : 
    * @Return : 
    */
    
    public void redirectToIndex() {
    
    
         String url = "https://localhost:11488/aussietravels-web/faces/public/index.xhtml";
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(Navigation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
    
    
    private Boolean validateYearAndMonth () {

        try {
        
            int parsedYear = Integer.parseInt(year);
            int parsedMonth = Integer.parseInt(month);
        
            if (parsedYear < 2018) {
            
                FacesContext.getCurrentInstance().addMessage("payment-form:year", new FacesMessage("Invalid year","Invalid year"));
                return false;
                
            
            
            }
            else if (parsedYear == 2018 && parsedMonth < 11) {
            
                 FacesContext.getCurrentInstance().addMessage("payment-form:year", 
                         new FacesMessage("Invalid year and month combination","Invalid year and month combination"));
                return false;
            
            
            }
            
            else if (parsedYear > 2018 && parsedYear < 2025) {
            
                
                return true;
            
            
            }
            else if (parsedYear > 2025) {
            
                FacesContext.getCurrentInstance().addMessage("payment-form:year", 
                         new FacesMessage("Invalid year ","Invalid year"));
            
                return false;
            
            }
            
            
            

        }
        
        
        catch (Exception ex) {
        
              FacesContext.getCurrentInstance().addMessage("payment-form:year", 
                         new FacesMessage("Invalid year and month combination","Invalid year and month combination"));
            return false;
        
        
        }


        return true;

    }
    
    
    
  
    
    
    // Getters and setters
    
      public ResortController getResortController() {
        return resortController;
    }

    public void setResortController(ResortController resortController) {
        this.resortController = resortController;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    
     public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
     public PaymentDAO getPaymentDAOImpl() {
        return paymentDAOImpl;
    }

    public void setPaymentDAOImpl(PaymentDAO paymentDAOImpl) {
        this.paymentDAOImpl = paymentDAOImpl;
    }
    
     public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    

    
    
    
    
    
}
