/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.entities.Users;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jishnu
 */
@Named(value = "userDetailsController")
@RequestScoped
public class UserDetailsController {

   
    
    private String pageTitle;
    private String userId;
    private Users user;
    private boolean resultAvailable;
    
    @EJB
    private UserSearch userSearchimpl;
    
    /**
     * Creates a new instance of UserDetailsController
     */
    public UserDetailsController() {
    }
    
    
    
    
    @PostConstruct
    public void init() {
    
        //TODO : PUT TRY EXCEPT AND ACCESS CONTROL 
        userId = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("userDetails");
        
        
         
         
         
         
         if (userId == null ) {
         
            
             resultAvailable = false;
             
         
         }
         
         else {
         
              user = getUserDetails();
              
              if (user == null) {
              
                resultAvailable = false;
              }
              
              resultAvailable = true;
             
         }
         
         
         
    }
    
    
    /*
    * This method returns the details of the user based on the user ID
    * @Param : 
    * @Return : 
    */
     private Users getUserDetails() {
        
         Users resultUser = userSearchimpl.getUserById(userId);
         
         return resultUser;
         
         
         
     }
    
    
    // Getters and setters
     public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

   

    public boolean isResultAvailable() {
        return resultAvailable;
    }

    public void setResultAvailable(boolean resultAvailable) {
        this.resultAvailable = resultAvailable;
    }



   

    
    
}
