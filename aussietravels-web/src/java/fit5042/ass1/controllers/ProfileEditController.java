/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.controllers;

import fit5042.ass1.entities.UserType;
import fit5042.ass1.entities.Users;
import fit5042.ass1.userType.UserTypeDAO;
import fit5042.ass1.users.UserSearch;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;


/**
 *
 * @author Jishnu
 */
@Named(value = "profileEditController")
@RequestScoped
public class ProfileEditController {

 private String pageTitle;
    private Long userId;
    private String id;

    private Users user;
   
    private boolean resultAvailable;
    
   

    @NotNull
    @Size(min = 2, max = 30, message = " First Name should be at least 2 letters long")
    @Pattern(regexp = "[A-Z][a-z]+", message = "The first name should start with capital letters and should have only alphabets " )
    private String firstName;
    
    @NotNull
    @Size(min = 2, max = 30,  message = " Last Name should be at least 2 letters long")
    @Pattern(regexp = "[A-Z][a-z]+", message = "The last name should start with capital letters and should have only alphabets " )
    private String lastName;
    
    @NotNull
    @Size(min = 5, max = 30,  message = "invalid email")
    @Email
    private String email;
   
   
    private String phoneNumber;
    
    
    @NotNull
    @Size(min = 10, message = "Please enter a valid address")
    private String address;

    @NotNull
    @Pattern(regexp = "public|admin", message = "Please specify the user as either admin or public")
    private String userTypeText;
    
    @EJB
    private UserSearch userSearchImpl;
    
    
    @EJB
    private UserTypeDAO userTypeDAOimpl;
    
    
    /**
     * Creates a new instance of UserEditController
     */
    
    
    @PostConstruct
    public void init(){
    
        
        id = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("editDetails");
        
        if (id == null) {
         
            
             System.out.println("Post Request");
                
         }
         
         else {
                 
                 
            user = getUserDetails();
         
            if (user == null ) {
         
            
             resultAvailable = false;
             
             
         
            }

             else {
         
          
              resultAvailable = true;
              setvalues();
             
                 }
                 
          
                 }

    }
    
    
    
    
     /*
    * This method is responsible for populating the variables of the bean that
    are binded with JSF pages
    * @Param : 
    * @Return : 
    */
     private void setvalues() {
     
         
         userId = user.getUserId();
         firstName = user.getFirstName();
         lastName = user.getLastName();
         email = user.getEmail();
         phoneNumber = user.getPhoneNumber();
         address = user.getStreetAddress();
         
         if (user.getUserType().getId() == 1) {
         
             userTypeText = "admin";
         
         }
         else {
         
             userTypeText = "public";
         
         }
  
     
     }
    
    
     
      /*
    * This method cals the ejb to get the correct user details
    * @Param : 
    * @Return : 
    */
     private Users getUserDetails() {
        
         Users resultUser = userSearchImpl.getUserById(id);
         
         return resultUser;
  
     }
    
     
     
     
      /*
    * This method calls the ejb to update the user details based on the 
     input provided
    * @Param : 
    * @Return : 
    */
     
     public void updateUsers() {
         
        try { 
            userId = Long.parseLong(id);
            UserType currentUserType = null;
         
            
            if (userTypeText == null) {
            
            currentUserType = userTypeDAOimpl.getUserType("2");
            }
            else if (userTypeText.equalsIgnoreCase("Admin")) {
       
            currentUserType = userTypeDAOimpl.getUserType("1");
            }
            else {
             currentUserType = userTypeDAOimpl.getUserType("2");
     
            }
       
       
            if (currentUserType != null) {
       
                userSearchImpl.updateUsers(userId, firstName, lastName, phoneNumber, email, address, currentUserType);
     
             }
            
            
             try {
                 String url = "https://localhost:11488/aussietravels-web/faces/public/editDetails.xhtml?editDetails=" + id ;
                 FacesContext.getCurrentInstance().getExternalContext().redirect(url);
            } catch (IOException ex1) {
              Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
            
            
            
        }
        catch (Exception ex) {
        
        
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("https://localhost:11488/aussietravels-web/faces/public/error.xhtml");
            } catch (IOException ex1)  {
                
                
                Logger.getLogger(UserEditController.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
       
       resultAvailable = true;
       
 
     }
     
  
     
    // Getters and setters
    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public boolean isResultAvailable() {
        return resultAvailable;
    }

    public void setResultAvailable(boolean resultAvailable) {
        this.resultAvailable = resultAvailable;
    }
    
     public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {

            this.phoneNumber = phoneNumber;
 
    }

    

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
    public String getUserTypeText() {
        return userTypeText;
    }

    public void setUserTypeText(String userTypeText) {
        this.userTypeText = userTypeText;
    }

    
    // id for query param
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
      /**
     * Creates a new instance of profileEditController
     */
    public ProfileEditController() {
    }
    
}
