angular.module('ngCompare')
    .controller('myCtrl', ['$scope', 'packageService', function($scope, packageService){
        $scope.packages = [];
        $scope.count =0;
        $scope.compare = []
        packageService.getAllPackages().then(
                function(data){
                     data.forEach(function(item){
                         
                         var package = {};
                         package.name = item.packageName;
                         package.id = item.packageId;
                         $scope.packages.push(package);
                         
                         
                     });
                     
                   
                    
                }, function(error){
                    $scope.errorResponse = error.statusText;
                });


         
             
           $scope.comparePackages = function(){
               
            var query_1 = $scope.package1; 
            var query_2 = $scope.package2;
            
            if (!query_1 || !query_2) {
                
                console.log("error");
                
            }
            else {
                
              $scope.compare = []

            packageService.getPackageDetails(query_1).then(
                function(data){
                   // console.log(data);
                    var package1 = {};
                    
                    package1.name = data.packageName;
                    package1.resortName = data.associatedResort.resortName;
                    package1.streetAddress = data.associatedResort.streetAddress;
                    package1.city = data.associatedResort.city;
                    package1.cost = data.packageCost;
                    package1.act1 = data.scheduledActivity1.activityName;
                    package1.act2 = data.scheduledActivity2.activityName;
                    package1.act3 = data.scheduledActivity3.activityName;
    
                    $scope.compare.push(package1);
                        
                        
                         packageService.getPackageDetails(query_2).then(
                function(data){
                   // console.log(data);
                    var package2 = {};
                    console.log(data);
                    
                    package2.name = data.packageName;
                    package2.resortName = data.associatedResort.resortName;
                    package2.streetAddress = data.associatedResort.streetAddress;
                    package2.city = data.associatedResort.city;
                    package2.cost = data.packageCost;
                    package2.act1 = data.scheduledActivity1.activityName;
                    package2.act2 = data.scheduledActivity2.activityName;
                    package2.act3 = data.scheduledActivity3.activityName;
    
                    $scope.compare.push(package2);
    
                    
                }, function(error){
                    $scope.errorResponse = error.statusText;
                });
    
                    
                }, function(error){
                    $scope.errorResponse = error.statusText;
                });
            }
               
               
               
           }

        
    }]);