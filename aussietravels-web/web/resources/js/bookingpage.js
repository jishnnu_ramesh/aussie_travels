/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    
    $('.data-table-selector').DataTable();
    
    
     $(".search-text").attr('placeholder','Enter Your Booking ID');
    
    
     var item = document.querySelector(".search-by-select-menu");
     
     if (item.value == 2) {
         
             $(".search-text").attr('placeholder','Enter Your Booking Type');
             $(".search-text").attr("disabled",true);
             $(".booking-search-div").css('display','none');  
             $(".booking-type-drop-down").css('display','block');
         
     } 
     
     if (item.value == 3) {
         
             $(".search-text").attr('placeholder','Enter Your Booking Name');
             $(".booking-type-drop-down").css('display','none');
             $(".booking-search-div").css('display','block');
             $(".search-text").removeAttr("disabled");
     } 
    
    
    $(".search-by-select-menu").on('change', function (){
       
        if (this.value == 1) {
            
            $(".search-text").attr('placeholder','Enter Your Booking ID');
            $(".booking-type-drop-down").css('display','none');
            $(".booking-search-div").css('display','block');
            $(".search-text").removeAttr('disabled');
            

        }
        
        else if (this.value == 2) {
            
             $(".search-text").attr('placeholder','Enter Your Booking Type');
             $(".search-text").attr("disabled",true);
             $(".booking-search-div").css('display','none');  
             $(".booking-type-drop-down").css('display','block');
              
        }
        
        else if (this.value == 3) {
            
             $(".search-text").attr('placeholder','Enter Your Booking Name');
             $(".booking-type-drop-down").css('display','none');
             $(".booking-search-div").css('display','block');
             $(".search-text").removeAttr("disabled");
        }
        
    });
});


