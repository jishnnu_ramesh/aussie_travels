/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// for all sliders
let sliders;

//sets all sliders to display none
function reset() {

   
    for (let i = 1; i < sliders.length; i++) {

        sliders[i].style.display = "none";
        console.log(sliders[i]);
    }

}

function animation() {

    let index = 0;


    setInterval(function () {


        sliders[index].style.display = "none";
        index = (index + 1) % 3;
        sliders[index].style.display = "block";


    },10000);


}



//init function
function startSlide() {
    sliders = document.querySelectorAll('.slide');
    reset();
    animation();
}

document.onload = startSlide();



$('select[name="activity"]').change(function(){
  
    if ($(this).val() === "1") {
     $(".resort-form").css('display','block');
     $(".resort-text").css('display','block');
     $(".package-form").css('display','none');
     $(".package-text").css('display','none');
     $(".activity-form").css('display','none');
     $(".activity-text").css('display','none');
          
    }
    
    if ($(this).val() === "2") {
     $(".package-form").css('display','block');
     $(".package-text").css('display','block');
     $(".activity-form").css('display','none');
     $(".activity-text").css('display','none')
     $(".resort-form").css('display','none');
     $(".resort-text").css('display','none');
    }
    
    if ($(this).val() === "3") {
     $(".activity-form").css('display','block');
     $(".activity-text").css('display','block');
     $(".resort-form").css('display','none');
     $(".resort-text").css('display','none');
     $(".package-form").css('display','none');
     $(".package-text").css('display','none');
          
    }
    
    });