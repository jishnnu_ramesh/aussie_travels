angular.module('ngCompare').
factory('packageService',['$http','$q', function($http, $q){
    var service = {
        //pokemonResultList: [],
        //count: 0,
        //pokemonResult: {},
        getAllPackages : getAllPackages,
        getPackageDetails : getPackageDetails,
        //get20PokemonResults : get20PokemonResults
    };

    return service;

    /*function get20PokemonResults(){
        var deferred =$q.defer();

        $http({
            method : "get",
            url : "http://pokeapi.co/api/v2/pokemon"
        }).
        then(function(resultList){
            pokemonResults = resultList.results;
            count = resultList.count;
            deferred.resolve(resultList);
        }, function(response){
            deferred.reject(response || "Failed to get Pokemon results");
        });

        return deferred.promise;
    };*/

    function getAllPackages(){
        var deferred =$q.defer();

        $http({
            method : "get",
            url : "http://localhost:11487/aussietravels-ws/webresources/fit5042.ass1.packagews.packages/"
        }).
        then(function(result){
            deferred.resolve(result.data);
        }, function(response){
            console.log(response);
            deferred.reject(response);
        });

        return deferred.promise;
    };
    
    function getPackageDetails(idOrName){
        var deferred =$q.defer();

        $http({
            method : "get",
            url : "http://localhost:11487/aussietravels-ws/webresources/fit5042.ass1.packagews.packages/" + idOrName
        }).
        then(function(result){
            deferred.resolve(result.data);
        }, function(response){
            console.log(response);
            deferred.reject(response);
        });

        return deferred.promise;
    };
    
}]);