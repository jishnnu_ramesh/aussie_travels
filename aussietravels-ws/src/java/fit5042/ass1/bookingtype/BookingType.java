/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.bookingtype;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "BOOKING_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingType.findAll", query = "SELECT b FROM BookingType b")
    , @NamedQuery(name = "BookingType.findByBookingId", query = "SELECT b FROM BookingType b WHERE b.bookingId = :bookingId")
    , @NamedQuery(name = "BookingType.findByTypeName", query = "SELECT b FROM BookingType b WHERE b.typeName = :typeName")
    , @NamedQuery(name = "BookingType.findBySurcharges", query = "SELECT b FROM BookingType b WHERE b.surcharges = :surcharges")})
public class BookingType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "BOOKING_ID")
    private Long bookingId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "TYPE_NAME")
    private String typeName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SURCHARGES")
    private BigDecimal surcharges;

    public BookingType() {
    }

    public BookingType(Long bookingId) {
        this.bookingId = bookingId;
    }

    public BookingType(Long bookingId, String typeName) {
        this.bookingId = bookingId;
        this.typeName = typeName;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public BigDecimal getSurcharges() {
        return surcharges;
    }

    public void setSurcharges(BigDecimal surcharges) {
        this.surcharges = surcharges;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingId != null ? bookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingType)) {
            return false;
        }
        BookingType other = (BookingType) object;
        if ((this.bookingId == null && other.bookingId != null) || (this.bookingId != null && !this.bookingId.equals(other.bookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.bookingtype.BookingType[ bookingId=" + bookingId + " ]";
    }
    
}
