/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.packagews;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "ACTIVITY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activity.findAll", query = "SELECT a FROM Activity a")
    , @NamedQuery(name = "Activity.findByActivityId", query = "SELECT a FROM Activity a WHERE a.activityId = :activityId")
    , @NamedQuery(name = "Activity.findByActivityName", query = "SELECT a FROM Activity a WHERE a.activityName = :activityName")
    , @NamedQuery(name = "Activity.findByActivityLocation", query = "SELECT a FROM Activity a WHERE a.activityLocation = :activityLocation")
    , @NamedQuery(name = "Activity.findByActivityCost", query = "SELECT a FROM Activity a WHERE a.activityCost = :activityCost")
    , @NamedQuery(name = "Activity.findByCostReduction", query = "SELECT a FROM Activity a WHERE a.costReduction = :costReduction")})
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVITY_ID")
    private Long activityId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ACTIVITY_NAME")
    private String activityName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ACTIVITY_LOCATION")
    private String activityLocation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVITY_COST")
    private BigDecimal activityCost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COST_REDUCTION")
    private BigDecimal costReduction;
    @OneToMany(mappedBy = "scheduledActivity3")
    private Collection<Packages> packagesCollection;
    @OneToMany(mappedBy = "scheduledActivity2")
    private Collection<Packages> packagesCollection1;
    @OneToMany(mappedBy = "scheduledActivity1")
    private Collection<Packages> packagesCollection2;

    public Activity() {
    }

    public Activity(Long activityId) {
        this.activityId = activityId;
    }

    public Activity(Long activityId, String activityName, String activityLocation, BigDecimal activityCost, BigDecimal costReduction) {
        this.activityId = activityId;
        this.activityName = activityName;
        this.activityLocation = activityLocation;
        this.activityCost = activityCost;
        this.costReduction = costReduction;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityLocation() {
        return activityLocation;
    }

    public void setActivityLocation(String activityLocation) {
        this.activityLocation = activityLocation;
    }

    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public BigDecimal getCostReduction() {
        return costReduction;
    }

    public void setCostReduction(BigDecimal costReduction) {
        this.costReduction = costReduction;
    }

    @XmlTransient
    public Collection<Packages> getPackagesCollection() {
        return packagesCollection;
    }

    public void setPackagesCollection(Collection<Packages> packagesCollection) {
        this.packagesCollection = packagesCollection;
    }

    @XmlTransient
    public Collection<Packages> getPackagesCollection1() {
        return packagesCollection1;
    }

    public void setPackagesCollection1(Collection<Packages> packagesCollection1) {
        this.packagesCollection1 = packagesCollection1;
    }

    @XmlTransient
    public Collection<Packages> getPackagesCollection2() {
        return packagesCollection2;
    }

    public void setPackagesCollection2(Collection<Packages> packagesCollection2) {
        this.packagesCollection2 = packagesCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (activityId != null ? activityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activity)) {
            return false;
        }
        Activity other = (Activity) object;
        if ((this.activityId == null && other.activityId != null) || (this.activityId != null && !this.activityId.equals(other.activityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.packagews.Activity[ activityId=" + activityId + " ]";
    }
    
}
