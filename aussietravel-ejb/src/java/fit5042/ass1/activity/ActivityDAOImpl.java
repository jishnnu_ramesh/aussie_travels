/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.activity;

import fit5042.ass1.entities.Activity;
import fit5042.ass1.util.ActivityResultList;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jishnu
 */
@Stateless
public class ActivityDAOImpl implements ActivityDAO{

    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
    
    
    
    /*
    * This method returns all activities available from the activities table in the database
    * @Param : 
    * @Return : ArrayList of Activity
    */
    @Override
    public ArrayList<Activity> getAllActivities() {
    
        TypedQuery query = entityManager.createNamedQuery("Activity.findAll", Activity.class);
        ArrayList<Activity> resultList = new ArrayList<Activity> (query.getResultList());
        return  resultList;
        
    }

    
    
     /*
    * This method returns ana ctivity for a specific id
    * @Param : String id
    * @Return : Activity object
    */
    @Override
    public Activity findActivityById(String id) {

        TypedQuery query = entityManager.createNamedQuery("Activity.findByActivityId", Activity.class);
        
        try{
        
            Long Id = Long.parseLong(id);
            query.setParameter("activityId", Id);
            return (Activity) query.getSingleResult();
        
        }
        catch (Exception ex) {
            return null;
        }
    
        


    }

  
}
