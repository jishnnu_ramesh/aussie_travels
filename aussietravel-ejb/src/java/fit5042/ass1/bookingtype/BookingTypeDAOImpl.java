
package fit5042.ass1.bookingtype;


import static com.sun.xml.ws.api.message.Packet.State.ClientResponse;
import fit5042.ass1.entities.BookingType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.WebTarget;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

/**
 *
 * Implementation of Booking Type DAO   
 * @author Jishnu
 */
@Stateless
public class BookingTypeDAOImpl implements BookingTypeDAO {

   @PersistenceContext (unitName = "aussietravel-ejbPU")
   private EntityManager entityManager;
    
    
    
    /*
    * This method returns a booking type for a particular id
    * @Param : int id
    * @Return : BookingType
    */
    @Override
    public BookingType getBookingType(int id) {
        
        TypedQuery query = entityManager.createNamedQuery("BookingType.findByBookingId",
                BookingType.class);
        
        
        try {
        
            query.setParameter("bookingId",id);
            return  (BookingType) query.getSingleResult();
        
        }
        catch (Exception ex) {
        
            return null;
        
        }
        
    
    }

   
    
    
     /*
    * This method returns an arraylist of all  booking type
    * @Param : 
    * @Return : ArrayList of BookingType
    */
    @Override
    public ArrayList<BookingType> getAllBookingTypes() {
    
        TypedQuery query = entityManager.createNamedQuery("BookingType.findAll", BookingType.class);
        
        return  new ArrayList<BookingType> (query.getResultList());
    
    
    }

    
    
     /*
    * This method returns a hashmap of String, String which corresponds 
    to the booking type that is obtained from webservice
    * @Param : 
    * @Return : HashMap<String,String>
    */
    @Override
    public HashMap<String,String> getAllBookingTypesFormWebService() {
    
       BookingTypeFacadeREST_JerseyClient myWs = new BookingTypeFacadeREST_JerseyClient();
       String response = myWs.findAll_JSON(String.class);
    
       HashMap<String, String> resultMap = new HashMap<String,String>();
       
       JSONArray jsonArray = null;
       
       try {
           jsonArray = new JSONArray(response.toString());
           JSONObject jsonObject = null;
           for (int i = 0; i < jsonArray.length(); i++) {
           
               jsonObject = jsonArray.getJSONObject(i);
               String typeName = jsonObject.getString("typeName");
               String bookingId = jsonObject.getString("bookingId");
               //String surcharges = jsonObject.getString("surcharges");
               resultMap.put(typeName,bookingId);
               
           
           }
           
           
       } catch (JSONException ex) {
           Logger.getLogger(BookingTypeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
     
       
       
    
        return resultMap;
    }
    
    
    
    
    
    static class BookingTypeFacadeREST_JerseyClient {

        private WebTarget webTarget;
        private Client client;
        private static final String BASE_URI = "http://localhost:11487/aussietravels-ws/webresources";

        public BookingTypeFacadeREST_JerseyClient() {
            client = javax.ws.rs.client.ClientBuilder.newClient();
            webTarget = client.target(BASE_URI).path("fit5042.ass1.bookingtype.bookingtype");
        }

        public String countREST() throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path("count");
            return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
        }

        public void edit_XML(Object requestEntity, String id) throws ClientErrorException {
            webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request(javax.ws.rs.core.MediaType.APPLICATION_XML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML));
        }

        public void edit_JSON(Object requestEntity, String id) throws ClientErrorException {
            webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
        }

        public <T> T find_XML(Class<T> responseType, String id) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
        }

        public <T> T find_JSON(Class<T> responseType, String id) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
        }

        public <T> T findRange_XML(Class<T> responseType, String from, String to) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{from, to}));
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
        }

        public <T> T findRange_JSON(Class<T> responseType, String from, String to) throws ClientErrorException {
            WebTarget resource = webTarget;
            resource = resource.path(java.text.MessageFormat.format("{0}/{1}", new Object[]{from, to}));
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
        }

        public void create_XML(Object requestEntity) throws ClientErrorException {
            webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_XML).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_XML));
        }

        public void create_JSON(Object requestEntity) throws ClientErrorException {
            webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
        }

        public <T> T findAll_XML(Class<T> responseType) throws ClientErrorException {
            WebTarget resource = webTarget;
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
        }

        public <T> T findAll_JSON(Class<T> responseType) throws ClientErrorException {
            WebTarget resource = webTarget;
            return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
        }

        public void remove(String id) throws ClientErrorException {
            webTarget.path(java.text.MessageFormat.format("{0}", new Object[]{id})).request().delete();
        }

        public void close() {
            client.close();
        }
    }
    
    
    
    
    
    
    
}
