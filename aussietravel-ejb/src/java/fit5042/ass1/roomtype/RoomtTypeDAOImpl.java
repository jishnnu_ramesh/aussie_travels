/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.roomtype;

import fit5042.ass1.entities.Rooms;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jishnu
 */
@Stateless
public class RoomtTypeDAOImpl  implements RoomTypeDAO{
    
    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;

    
    
     /*
    * This method returns an arrayList of all rooms in the database
    * @Param :  
    * @Return : ArrayList of all rooms
    */
    @Override
    public ArrayList<Rooms> getAllRooms() {

        
        TypedQuery query = entityManager.createNamedQuery("Rooms.findAll", Rooms.class);
        ArrayList<Rooms> resultList = new ArrayList<Rooms> (query.getResultList());

        return resultList;

    }

    
    
    
     /*
    * This method returns a Room for a specific roomid
    * @Param :  String RoomId
    * @Return : Rooms
    */
    @Override
    public Rooms getRoomById(String roomId) {
    
       try{
       
        int id = Integer.parseInt(roomId);
        TypedQuery query = entityManager.createNamedQuery("Rooms.findByRoomId", Rooms.class);
        query.setParameter("roomId", id);
        Rooms room = (Rooms) query.getSingleResult();
        return room;
           
       }
       catch (Exception ex) {
       
       
           return null;
       
       }
        
        
       
    
    
    
    }

   

    
    
    
    
}
