/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.resort;

import fit5042.ass1.entities.Resorts;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jishnu
 */
@Stateless
public class ResortDAOImpl implements ResortDAO {

    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
    
     /*
    * This method returns a Resort for which the id matches
    * @Param :  int id
    * @Return : Resorts
    */
    @Override
    public Resorts getResortById(int Id) {

        TypedQuery query = entityManager.createNamedQuery("Resorts.findByResortId", Resorts.class);
        
        try{
        
            query.setParameter("resortId", Id);
            return (Resorts) query.getSingleResult();
        
        }
        catch (Exception ex) {
            return null;
        }
        
    }

   

     /*
    * This method returns an ArrayList of  all resorts
    * @Param : 
    * @Return : ArrayList of Resorts
    */
    @Override
    public ArrayList<Resorts> getAllResorts() {
    
        TypedQuery query = entityManager.createNamedQuery("Resorts.findAll", Resorts.class);
        ArrayList<Resorts> resortResults = new ArrayList<Resorts>(query.getResultList());
        return resortResults;
 
    }

   

    
    
}
