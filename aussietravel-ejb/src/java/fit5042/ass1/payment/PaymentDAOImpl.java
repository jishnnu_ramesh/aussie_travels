/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.payment;

import fit5042.ass1.entities.CreditCardPayment;
import fit5042.ass1.entities.Voucher;
import java.math.BigDecimal;
import java.math.MathContext;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jishnu
 */
@Stateless

public class PaymentDAOImpl implements PaymentDAO {

    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
    
    
    
     /*
    * This method returns a long denoting the id for the new payment row created in the database
    * @Param :  String Name of the card holder, double cost
    * @Return : Long
    */
    @Override
    public Long creditCardPaymentImpl(String name, double cost) {
    
        try {
        CreditCardPayment creditCardPayment = new CreditCardPayment();
         BigDecimal value = new BigDecimal(cost,MathContext.DECIMAL32);
         BigDecimal roundOff = value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        creditCardPayment.setAmount(roundOff);
        creditCardPayment.setPaymentType('C');
        creditCardPayment.setCardHolderName(name);
        //creditCardPayment.setId((long)0);
       
        entityManager.persist(creditCardPayment);
        entityManager.flush();
        return creditCardPayment.getId();
        }
        catch (Exception ex) {
        
            return 0L;
        
        
        }
    
    }

    
    
    
     /*
    * This method returns a long denoting the id for the new voucher payment row created in the database
    * @Param :  String voucher code
    * @Return : Long
    */
    @Override
    public Long voucherPayment(String voucherCode) {
    
        try {
        
            Voucher voucher = new Voucher();
            voucher.setAmount(BigDecimal.ZERO);
            voucher.setVoucherCode(voucherCode);
            voucher.setPaymentType('v');
            entityManager.persist(voucher);
            entityManager.flush();
            return voucher.getId();
        
        }
        catch (Exception ex) {
        
            return 0L;
        
        
        }
    
    
    
    }

   
}
