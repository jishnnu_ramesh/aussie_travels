/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.booking;

import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.BookingType;
import fit5042.ass1.entities.PackageBooking;
import fit5042.ass1.entities.Packages;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.Resorts;
import fit5042.ass1.entities.Rooms;
import fit5042.ass1.entities.Users;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jishnu
 */
@Stateless

public class BookingDAOImpl implements BookingDAO {
    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    

  

     /*
    * This method save a resort booking
    * @Param : String transactionName, Resorts resort, Rooms roomTypeSelection, String numOfPeople, 
            int numOfRooms, String airportPickup, String bookingType, double price, Users user, String date
    * @Return : long - Booking Id
    */
    @Override
    public long saveResortBooking(String transactionName, Resorts resort, Rooms roomTypeSelection, String numOfPeople, 
            int numOfRooms, String airportPickup, String bookingType, double price, Users user, String date) {

        try{
        
            ResortBooking resortBooking = new ResortBooking();
        
            resortBooking.setTransactionName("RESORT BOOKING " + transactionName.toUpperCase());
            resortBooking.setResortId(resort);
            resortBooking.setRoomType(roomTypeSelection);
            int numbOfPeople = Integer.parseInt(numOfPeople);
            resortBooking.setNumOfPeople((short) numbOfPeople);
            resortBooking.setNumOfRooms((short) numOfRooms);
            resortBooking.setUserId(user);

            if (airportPickup.equals("0")) {
            
                resortBooking.setAddOnService(Boolean.FALSE);
            
            }
            else {
            
                resortBooking.setAddOnService(Boolean.TRUE);
            
            }
            
            BigDecimal cost = new BigDecimal(price,MathContext.DECIMAL32);
            BigDecimal roundOff = cost.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            resortBooking.setTotalCost(roundOff);
            //https://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
            java.util.Date date_util = sdf1.parse(date);
            java.sql.Date sqlStartDate = new java.sql.Date(date_util.getTime());  
            resortBooking.setDateOfBooking(sqlStartDate);
            

            resortBooking.setBookingType(entityManager.find(BookingType.class, Long.parseLong(bookingType)));
            
            entityManager.persist(resortBooking);
            
            entityManager.flush();
            return resortBooking.getResortBookingId();
        
        
        }
        catch (Exception ex) {
        
            return 0L;
            
            
          }




    }

    
     /*
    * This method save a package booking
    * @Param : String transactionName, Packages packages, String numOfPeople,
            String bookingType, double price, Users user, String date
    * @Return : long - Booking Id
    */
    @Override
    public long savePackageBooking(String transactionName, Packages packages, String numOfPeople,
            String bookingType, double price, Users user, String date) {
    
        
        try{
        
            PackageBooking packageBooking = new PackageBooking();
            packageBooking.setTransactionName("PACKAGE BOOKING " +transactionName.toUpperCase());
            int numbOfPeople = Integer.parseInt(numOfPeople);
            packageBooking.setPackageId(packages);
            packageBooking.setNumOfPeople( (short) Integer.parseInt(numOfPeople));
            BigDecimal cost = new BigDecimal(price,MathContext.DECIMAL32);
            BigDecimal roundOff = cost.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            packageBooking.setPackageCost(roundOff);
            //https://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
             SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
            java.util.Date date_util = sdf1.parse(date);
            java.sql.Date sqlStartDate = new java.sql.Date(date_util.getTime());  
            packageBooking.setDateOfBooking(sqlStartDate);

            packageBooking.setBookingType(entityManager.find(BookingType.class, Long.parseLong(bookingType)));
            packageBooking.setUserId(user);
            
            entityManager.persist(packageBooking);
            entityManager.flush();
            return packageBooking.getPackageBookingId();
            
        
        
        }
        catch (Exception ex) {
        
        
            return 0L;
        
        
        }
    
    
    
    
    }

    
     /*
    * This method save an activity booking
    * @Param : String transactionName, Activity activity,
            String numOfPeople, String bookingType, double price, Users user, String date
    * @Return : long - Booking Id
    */
    @Override
    public long saveActivityBooking(String transactionName, Activity activity,
            String numOfPeople, String bookingType, double price, Users user, String date) {
    
        
        try{
        
            ActivtyBooking activityBooking = new ActivtyBooking ();
            activityBooking.setTransactionName("ACTIVITY BOOKING " +transactionName.toUpperCase());
            activityBooking.setActivityId(activity);
            activityBooking.setNumOfPeople( (short) Integer.parseInt(numOfPeople));
            BigDecimal cost = new BigDecimal(price,MathContext.DECIMAL32);
            BigDecimal roundOff = cost.setScale(2, BigDecimal.ROUND_HALF_EVEN);

            activityBooking.setActivityCost(roundOff);
            //https://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
            java.util.Date date_util = sdf1.parse(date);
            java.sql.Date sqlStartDate = new java.sql.Date(date_util.getTime());  
            activityBooking.setDateOfBooking(sqlStartDate);
            
            activityBooking.setBookingType(entityManager.find(BookingType.class, Long.parseLong(bookingType)));
            
            activityBooking.setUserId(user);
            
            
            entityManager.persist(activityBooking);
            entityManager.flush();
            return activityBooking.getActivityBookingId();
        
        
        }
        catch (Exception ex) {
        
        
            return 0L;
        
        
        }
    
    
    
    
    
    
    }
 

   

}