
package fit5042.ass1.booking;

import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.BookingType;
import fit5042.ass1.entities.PackageBooking;
import fit5042.ass1.entities.ResortBooking;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * This class acts as the DAO for all kinds of bookings made by the user
 * @author Jishnu
 */

@Stateless
public class BookingSearchImpl implements BookingSearch {
    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    

    
    
     /*
    * This method returns a arraylist of activity booking for a particular booking id
    * @Param : String bookingID
    * @Return : ArrayList of activity booking
    */
    @Override
    public ArrayList<ActivtyBooking> searchActivityById(String bookingId) {
    
        TypedQuery query = entityManager.createNamedQuery("ActivtyBooking.findByActivityBookingId", ActivtyBooking.class);
          
        try {
        
            Long id = Long.parseLong(bookingId);
            query.setParameter("activityBookingId", id);
            return (ArrayList<ActivtyBooking>) query.getResultList();
        
        }
        catch (Exception ex) {
        
            return new ArrayList<>();
        
        }
        
    }
    
    
    
      /*
    * This method returns an arraylist of package booking for a particular booking id
    * @Param : String bookingID
    * @Return : ArrayList of package booking
    */
    @Override 
    public ArrayList<PackageBooking> searchPackageById(String bookingId) {
    
         TypedQuery query = entityManager.createNamedQuery("PackageBooking.findByPackageBookingId", PackageBooking.class);
          
          try {
        
            Long id = Long.parseLong(bookingId);
            query.setParameter("packageBookingId", id);
            return (ArrayList<PackageBooking>) query.getResultList();
        
        }
        catch (Exception ex) {
        
            return new ArrayList<>();
        
        }
    }
    
    
    
      /*
    * This method returns an arraylist of resort booking for a particular booking id
    * @Param : String bookingId
    * @Return : ArrayList of resort booking
    */
    @Override
    public ArrayList<ResortBooking> searchResortById(String bookingId) {
        
     TypedQuery query = entityManager.createNamedQuery("ResortBooking.findByResortBookingId", ResortBooking.class);
       
     try{
     
          Long id = Long.parseLong(bookingId);
          query.setParameter("resortBookingId", id);
          return (ArrayList<ResortBooking>) query.getResultList();
     }
     catch (Exception ex) {
     
         return new ArrayList<>();
         
     }
    
    
    }
    
    
    
      /*
    * This method returns an arraylist of resort booking for a particular booking id
    * @Param : String searchText
    * @Return : ArrayList of resort booking
    */
    @Override
    public ArrayList<ResortBooking> searchResortByTransactionName(String searchText) {

     TypedQuery query = entityManager.createNamedQuery("ResortBooking.findByTransactionName", 
             ResortBooking.class);
    
    try{
     
          query.setParameter("transactionName", searchText);
          return new ArrayList<ResortBooking> (query.getResultList());
     }
     catch (Exception ex) {
     
         return new ArrayList<>();
         
     }
        

    }

    
      /*
    * This method returns an arraylist of package booking for a particular transaction name
    * @Param : String searchText
    * @Return : ArrayList of package booking
    */
    @Override
    public ArrayList<PackageBooking> searchPackageByTransactionName(String searchText) {
    
        TypedQuery query = entityManager.createNamedQuery("PackageBooking.findByTransactionName", PackageBooking.class);
    
    try{
     
          query.setParameter("transactionName", searchText);
          ArrayList<PackageBooking> pk_booking =  new ArrayList<PackageBooking> (query.getResultList());
          return pk_booking;
     }
     catch (Exception ex) {
     
         return new ArrayList<>();
         
     }
    
    
    }

    
    
      /*
    * This method returns an arraylist of activity booking for a particulartransaction name
    * @Param : String searchText
    * @Return : ArrayList of activity booking
    */
    @Override
    public ArrayList<ActivtyBooking> searchActivityByTransactionName(String searchText) {
        
        TypedQuery query = entityManager.createNamedQuery("ActivtyBooking.findByTransactionName", 
             ActivtyBooking.class);
    
    try{
     
          query.setParameter("transactionName", searchText);
          return new ArrayList<ActivtyBooking> (query.getResultList());
     }
     catch (Exception ex) {
     
         return new ArrayList<>();
         
     }
        
    
    }

    
    
    
    
    
    
      /*
    * This method returns an arraylist of activity booking for a particular booking type
    * @Param : BookingType type
    * @Return : ArrayList of activity booking
    */
    @Override
    public ArrayList<ActivtyBooking> searchActivityByType(BookingType type) {
    
        
    TypedQuery query = entityManager.createNamedQuery("ActivtyBooking.findByType", 
             ActivtyBooking.class);
    
    
    try{
    
        query.setParameter("bookingType", type);
        return new ArrayList<ActivtyBooking> (query.getResultList());
        
    }
    catch (Exception ex) {
    
        
        return new ArrayList<>();
    
    }
    
    
    }

    
    
    
          /*
    * This method returns an arraylist of resort booking for a particular booking type
    * @Param : BookingType type
    * @Return : ArrayList of resort booking
    */
    @Override
    public ArrayList<ResortBooking> searchResortByType(BookingType type) {
    
        TypedQuery query = entityManager.createNamedQuery("ResortBooking.findByType", 
             ResortBooking.class);
    
    
    try{
    
        query.setParameter("bookingType", type);
        return new ArrayList<ResortBooking> (query.getResultList());
        
        }
    catch (Exception ex) {
    
        
        return new ArrayList<>();
    
    
        }
    }
    
    

    
          /*
    * This method returns an arraylist of package booking for a particular booking type
    * @Param : BookingType type
    * @Return : ArrayList of package booking
    */
    @Override
    public ArrayList<PackageBooking> searchPackageByType(BookingType type) {
    
        TypedQuery query = entityManager.createNamedQuery("PackageBooking.findByType", 
             PackageBooking.class);
    
    
    try{
 
        query.setParameter("bookingType", type);
        return new ArrayList<PackageBooking> (query.getResultList());
        
    }
    catch (Exception ex) {
    
        
        return new ArrayList<>();
    
    }

  
    
    }
}
