/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.packages;

import fit5042.ass1.entities.Packages;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jishnu
 */
@Stateless
public class PackagesDAOImpl implements PackagesDAO {

    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
  

     /*
    * This method returns an arralist of all packages 
    * @Param : 
    * @Return : ArrayList of packages
    */
    @Override
    public ArrayList<Packages> getAllPackages() {

        TypedQuery query = entityManager.createNamedQuery("Packages.findAll", Package.class);
        ArrayList<Packages> resultList = new ArrayList<Packages> (query.getResultList());
        return resultList;
    }

    
    
    
     /*
    * This method returns a package for a specific id
    * @Param : String id
    * @Return : Packages
    */
    @Override
    public Packages findPackageById(String id) {
    
         TypedQuery query = entityManager.createNamedQuery("Packages.findByPackageId", Packages.class);
        
        try{
        
            Long Id = Long.parseLong(id);
            query.setParameter("packageId", Id);
            return (Packages) query.getSingleResult();
        
        }
        catch (Exception ex) {
            return null;
        }
    
    
    }

   
}
