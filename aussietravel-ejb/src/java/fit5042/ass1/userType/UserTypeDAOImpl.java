/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.userType;

import fit5042.ass1.entities.UserType;
import fit5042.ass1.entities.Users;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jishnu
 */
@Stateless
public class UserTypeDAOImpl implements UserTypeDAO {

    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
    
    
     /*
    * This method returns the UserType for a specific user id
    * @Param :  String id
    * @Return : UserType
    */
    @Override
    public UserType getUserType(String id) {
        
        try {
        Short userId = Short.parseShort(id);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(UserType.class);
        Root<UserType> u = query.from(UserType.class);
        query.select(u);
        Predicate predicate = builder.equal(u.get("id").as(Short.class), userId);
        query.where(predicate);
        TypedQuery tQuery = entityManager.createQuery(query);
        return (UserType) tQuery.getSingleResult();
        }
        catch (Exception ex) {
        
            return null;
        
        
        }

    }

   
}
