/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.users;

import fit5042.ass1.entities.UserType;
import fit5042.ass1.entities.Users;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Jishnu
 */
@Stateless
public class UserDAOImpl implements UserDAO{

       
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;

    
    
    
     /*
    * This method deletes the user from the database and returns a boolean value
    indicating whether the user is deleted or not
    * @Param :  String id
    * @Return : Boolean
    */
    @Override
    public Boolean deleteUser(String userID) {

        try {
        
            Long id = Long.parseLong(userID);
            TypedQuery query = entityManager.createNamedQuery("Users.findByUserId", Users.class);
            query.setParameter("userId", id);
            Users user = (Users) query.getSingleResult();
            if (user == null) {
            
                return false;
            }
            else {
            
                entityManager.remove(user);
                return true;
            }
            
            
        
        }
        catch (Exception ex) {
        
            return false;
        
        
        }


    }

    
      /*
    * This method creates a new user in the database and returns a boolean value
    indicating whether the user is created or not
    * @Param :  String firstName, String lastName, String email, String password, String phone, String address, String userType
    * @Return : Boolean
    */
    @Override
    public Boolean createUser(String firstName, String lastName, String email, String password, String phone, String address, String userType) {
      

        
        // https://stackoverflow.com/questions/5531455/how-to-hash-some-string-with-sha256-in-java
        try {
            
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

		// bytes to hex
             StringBuilder sb = new StringBuilder();
              for (byte b : hashInBytes) {
                 sb.append(String.format("%02x", b));
              }
            
            short type = Short.parseShort(userType);
            UserType userTypeSelected = entityManager.find(UserType.class, type);
            
            
            
            Users user = new Users();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPhoneNumber(phone);
            user.setPassword(sb.toString());
            user.setStreetAddress(address);
            user.setUserType(userTypeSelected);
            
            entityManager.persist(user);
            return true;
        
        
        }
        catch (Exception ex) {
        
            Logger.getLogger("tet").log(Level.SEVERE, null, ex);
            return false;
        
        
        }
         

    }
    
    
    
    
    
}
