/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.users;

import fit5042.ass1.entities.UserType;
import fit5042.ass1.entities.Users;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Jishnu
 */
@Stateless
public class UserSearchImpl implements UserSearch{

    
    @PersistenceContext (unitName = "aussietravel-ejbPU")
    private EntityManager entityManager;
    
    
    
     /*
    * This method returns an arraylist of user having the given id and firstName
    * @Param :  String id, String FirstName
    * @Return : ArrayList of Users
    */
    @Override
    public ArrayList<Users> getUsersByIdAndFirstName(String id, String firstName) {
    
        TypedQuery query = entityManager.createNamedQuery("Users.findByIdAndFirstName",Users.class);
        
        Long userId = Long.parseLong(id);
        
        query.setParameter("userId",userId);
        query.setParameter("firstName", firstName);
        return new ArrayList<Users> (query.getResultList());
    
    }

    @Override
    public ArrayList<Users> getUsersByIdAndLastName(String id, String lastName) {
   
             
         TypedQuery query = entityManager.createNamedQuery("Users.findByIdAndLastName", Users.class);
         Long userId = Long.parseLong(id);
         query.setParameter("userId", userId);
         query.setParameter("lastName", lastName);
         return  new ArrayList<Users> (query.getResultList());
          
    
    }

    @Override
    public ArrayList<Users> getUsersByIdAndType(String id, String type) {
             
         TypedQuery query = entityManager.createNamedQuery("Users.findByIdAndType", Users.class);
         Long userId = Long.parseLong(id);
         query.setParameter("userId", userId);
         
         if (type.equalsIgnoreCase("Admin")) {
            query.setParameter("userType", 1);
         }
         else {
             query.setParameter("userType", 2); 
         }
         
         
         return new ArrayList<Users> (query.getResultList());
             
    
    
    
    }

    @Override
    public ArrayList<Users> getUsersByIdAndEmail(String id, String email) {
    
     
         TypedQuery query = entityManager.createNamedQuery("Users.findByIdAndEmail", Users.class);
         Long userId = Long.parseLong(id);
         query.setParameter("userId", userId);
         query.setParameter("email", email);
         return new ArrayList<Users> (query.getResultList());
   
    
    }

    @Override
    public ArrayList<Users> getUsersByLastNameAndFirstName(String lastName, String firstName) {
 
         TypedQuery query = entityManager.createNamedQuery("Users.findByFirstNameAndLastName", Users.class);
         query.setParameter("firstName", firstName);
         query.setParameter("lastName", lastName);
         return new ArrayList<Users> (query.getResultList());

    }

    @Override
    public ArrayList<Users> getUsersByLastNameAndType(String lastName, String type) {
   
                 
        TypedQuery query = entityManager.createNamedQuery("Users.findByLastNameAndType", Users.class);
         query.setParameter("lastName", lastName);
         
         if (type.equalsIgnoreCase("Admin")) {
            query.setParameter("userType", 1);
         }
         else {
             query.setParameter("userType", 2); 
         }
         
         
         return new ArrayList<Users> (query.getResultList());
    
    
    }

    @Override
    public ArrayList<Users> getUsersByLastNameAndEmail(String lastName, String email) {
        
         TypedQuery query = entityManager.createNamedQuery("Users.findByLastNameAndEmail", Users.class);
         query.setParameter("lastName", lastName);
         query.setParameter("email", email);
         return new ArrayList<Users> (query.getResultList());



    }

    @Override
    public ArrayList<Users> getUsersByFirstNameAndType(String firstName, String type) {

         TypedQuery query = entityManager.createNamedQuery("Users.findByFirstNameAndType", Users.class);
         query.setParameter("firstName", firstName);
         
         if (type.equalsIgnoreCase("Admin")) {
            query.setParameter("userType", 1);
         }
         else {
             query.setParameter("userType", 2); 
         }
         
         
         return new ArrayList<Users> (query.getResultList());
    
    
    }

    @Override
    public ArrayList<Users> getUsersByFirstNameAndEmail(String firstName, String email) {
    
   
         TypedQuery query = entityManager.createNamedQuery("Users.findByFirstNameAndEmail", Users.class);
         query.setParameter("firstName", firstName);
         query.setParameter("email", email);
         return new ArrayList<Users> (query.getResultList());

    }

    @Override
    public ArrayList<Users> getUsersByEmailAndType(String email, String type) {
      
      
    TypedQuery query = entityManager.createNamedQuery("Users.findByTypeAndEmail", Users.class);
         query.setParameter("email", email);
         
         if (type.equalsIgnoreCase("Admin")) {
            query.setParameter("userType", 1);
         }
         else {
             query.setParameter("userType", 2); 
         }
         
         
         return new ArrayList<Users> (query.getResultList());
    
    }

    @Override
    public Users getUserById(String id) {

        try {
        Long userId = Long.parseLong(id);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(Users.class);
        Root<Users> u = query.from(Users.class);
        query.select(u);
        Predicate predicate = builder.equal(u.get("userId").as(Long.class), userId);
        query.where(predicate);
        TypedQuery tQuery = entityManager.createQuery(query);
        return (Users) tQuery.getSingleResult();
        }
        catch (Exception ex) {
        
            return null;
        
        
        }
    }
    
    
    
    @Override
    public Users getUserById(Long id) {

        TypedQuery query = entityManager.createNamedQuery("Users.findByUserId", Users.class);
        query.setParameter("userId",id);
        return (Users) query.getSingleResult();
        
        
    }
    
    
    

    @Override
    public void updateUsers(Long userId, String firstName, String LastName, String phone, String email, String address, UserType userType) {
    
        Users currentUser = getUserById(userId);
        if (currentUser != null) {
        
            currentUser.setEmail(email);
            currentUser.setFirstName(firstName);
            currentUser.setLastName(LastName);
            currentUser.setPhoneNumber(phone);
            currentUser.setStreetAddress(address);
            currentUser.setUserType(userType);
        
        }
        
        entityManager.merge(currentUser);
    
    
    }

    @Override
    public Users getUserByEmail(String email) {
    
        try {
            
         TypedQuery query = entityManager.createNamedQuery("Users.findByEmail", Users.class);
         query.setParameter("email", email);
         return (Users) query.getSingleResult();
            
        }
        
        catch (Exception ex) {
        
        return null;
        
        }
    
    
    }

   
}
