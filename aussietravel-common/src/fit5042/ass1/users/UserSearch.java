/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.users;
import fit5042.ass1.entities.UserType;
import fit5042.ass1.entities.Users;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface UserSearch {
    
    public ArrayList<Users> getUsersByIdAndFirstName(String id, String firstName);
    public ArrayList<Users> getUsersByIdAndLastName(String id, String lastName);
    public ArrayList<Users> getUsersByIdAndType(String id, String type);
    public ArrayList<Users> getUsersByIdAndEmail(String id, String email);
    public ArrayList<Users> getUsersByLastNameAndFirstName(String lastName, String firstName);
    public ArrayList<Users> getUsersByLastNameAndType(String lastName, String type);
    public ArrayList<Users> getUsersByLastNameAndEmail(String lastName, String email);
    public ArrayList<Users> getUsersByFirstNameAndType(String firstName, String type);
    public ArrayList<Users> getUsersByFirstNameAndEmail(String firstName, String email);
    public ArrayList<Users> getUsersByEmailAndType(String email, String type);
    public Users getUserByEmail(String email);
    public Users getUserById(String id);
    public Users getUserById(Long id);
    public void updateUsers(Long userId, String firstName, String LastName, String phone, String email, String address, UserType userType );
   
    
}
