/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.users;

import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface UserDAO {
    
    public Boolean deleteUser(String userID);
    public Boolean createUser (String firstName,String lastName, String email, String password, String phone, String address, String userType);
    
}
