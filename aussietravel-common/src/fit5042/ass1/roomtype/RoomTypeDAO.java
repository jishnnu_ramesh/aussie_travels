/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.roomtype;

import fit5042.ass1.entities.Rooms;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface RoomTypeDAO {
    
   public ArrayList<Rooms> getAllRooms();
   
   public Rooms getRoomById(String roomId);
    
    
}
