/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.packages;

import fit5042.ass1.entities.Packages;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface PackagesDAO {
    
    public ArrayList<Packages> getAllPackages();
    public Packages findPackageById(String id);
    
}
