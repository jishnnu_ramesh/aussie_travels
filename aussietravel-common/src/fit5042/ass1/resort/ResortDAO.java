/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.resort;

import fit5042.ass1.entities.Resorts;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface ResortDAO {
    
    
    public Resorts getResortById(int Id);
    public ArrayList<Resorts> getAllResorts ();
   
}
