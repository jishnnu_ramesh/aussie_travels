/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.activity;

import fit5042.ass1.entities.Activity;
import fit5042.ass1.util.ActivityResultList;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface ActivityDAO {
    
    public ArrayList<Activity> getAllActivities();
    public Activity findActivityById(String id);
    
}
