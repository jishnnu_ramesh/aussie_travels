/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.util;

/**
 *
 * @author Jishnu
 */
public class BookingResultList {

    
    private long bookingId;
    private String bookingName;
    private String hyperlinkText;
    
    public BookingResultList(long bookingId, String bookingName, String hyperlinkText) {
        this.bookingId = bookingId;
        this.bookingName = bookingName;
        this.hyperlinkText = hyperlinkText;
    }

    public BookingResultList() {
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public String getHyperlinkText() {
        return hyperlinkText;
    }

    public void setHyperlinkText(String hyperlinkText) {
        this.hyperlinkText = hyperlinkText;
    }
    
    
    
    
    
}
