/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.util;

import java.util.ArrayList;

/**
 *
 * @author Jishnu
 */
public class ActivityResultList {

   
    
    private ArrayList<String> activityNames;
    private ArrayList<String> activityPlaces;
    
    
    
     public ArrayList<String> getActivityNames() {
        return activityNames;
    }

    public void setActivityNames(ArrayList<String> activityNames) {
        this.activityNames = activityNames;
    }

    public ArrayList<String> getActivityPlaces() {
        return activityPlaces;
    }

    public void setActivityPlaces(ArrayList<String> activityPlaces) {
        this.activityPlaces = activityPlaces;
    }
    
    
}
