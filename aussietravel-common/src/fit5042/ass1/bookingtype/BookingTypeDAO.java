
package fit5042.ass1.bookingtype;

import fit5042.ass1.entities.BookingType;
import java.util.ArrayList;
import java.util.HashMap;
import javax.ejb.Remote;
import javax.json.JsonArray;

/**
 * Remote interface for Booking Type DAO
 * @author Jishnu
 */
@Remote
public interface BookingTypeDAO {
    
    public BookingType getBookingType(int id);
    
    public ArrayList<BookingType> getAllBookingTypes();
    
    public HashMap<String,String> getAllBookingTypesFormWebService();
    
    
}
