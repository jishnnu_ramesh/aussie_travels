/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.payment;

import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface PaymentDAO {
    
    public Long creditCardPaymentImpl(String name, double cost);
    
    public Long voucherPayment(String voucherCode);
    
}
