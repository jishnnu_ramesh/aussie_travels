
package fit5042.ass1.booking;
import fit5042.ass1.entities.ActivtyBooking;
import fit5042.ass1.entities.BookingType;
import fit5042.ass1.entities.ResortBooking;
import fit5042.ass1.entities.PackageBooking;
import java.util.ArrayList;
import javax.ejb.Remote;

/**
 * Interface for booking search DAO
 * @author Jishnu
 */

@Remote
public interface BookingSearch {
    
    public ArrayList<ActivtyBooking> searchActivityById (String bookingId);
    
    public ArrayList<ResortBooking> searchResortById (String bookingId);
    
    public ArrayList<PackageBooking> searchPackageById (String bookingId);
    
    public ArrayList<ActivtyBooking> searchActivityByType(BookingType type);
    
    public ArrayList<ResortBooking> searchResortByType(BookingType type);
    
    public ArrayList<PackageBooking> searchPackageByType(BookingType type);

    public ArrayList<ResortBooking> searchResortByTransactionName(String searchText);

    public ArrayList<PackageBooking> searchPackageByTransactionName(String searchText);

    public ArrayList<ActivtyBooking> searchActivityByTransactionName(String searchText);
    
}
