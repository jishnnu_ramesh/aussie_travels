/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.booking;

import fit5042.ass1.entities.Activity;
import fit5042.ass1.entities.Packages;
import fit5042.ass1.entities.Resorts;
import fit5042.ass1.entities.Rooms;
import fit5042.ass1.entities.Users;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface BookingDAO {
    
     public long saveResortBooking(String transactionName, Resorts resort, Rooms roomTypeSelection, String numOfPeople,
            int numOfRooms, String airportPickup,  String bookingType, double price, Users user, String dat);
    
    public long savePackageBooking(String transactionName, Packages packages, String numOfPeople, 
            String bookingType, double price, Users user, String date);
    
    
    public long saveActivityBooking(String transactionName, Activity packages, String numOfPeople, 
            String bookingType, double price, Users user, String date );
    
    
    
    
}
