/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "ACTIVTY_BOOKING")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "ActivtyBooking.findAll", query = "SELECT a FROM ActivtyBooking a")
    , @NamedQuery(name = "ActivtyBooking.findByActivityBookingId", query = "SELECT a FROM ActivtyBooking a WHERE a.activityBookingId = :activityBookingId")
    , @NamedQuery(name = "ActivtyBooking.findByTransactionName", query = "SELECT a FROM ActivtyBooking a WHERE a.transactionName = :transactionName")
    , @NamedQuery(name = "ActivtyBooking.findByNumOfPeople", query = "SELECT a FROM ActivtyBooking a WHERE a.numOfPeople = :numOfPeople")
    , @NamedQuery(name = "ActivtyBooking.findByActivityCost", query = "SELECT a FROM ActivtyBooking a WHERE a.activityCost = :activityCost")
    , @NamedQuery(name = "ActivtyBooking.findByDateOfBooking", query = "SELECT a FROM ActivtyBooking a WHERE a.dateOfBooking = :dateOfBooking"),
     @NamedQuery(name = "ActivtyBooking.findByType", query = "SELECT a FROM ActivtyBooking a WHERE a.bookingType = :bookingType")})
public class ActivtyBooking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ACTIVITY_BOOKING_ID")
    private Long activityBookingId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TRANSACTION_NAME")
    private String transactionName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_OF_PEOPLE")
    private short numOfPeople;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVITY_COST")
    private BigDecimal activityCost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_OF_BOOKING")
    @Temporal(TemporalType.DATE)
    private Date dateOfBooking;
    @JoinColumn(name = "ACTIVITY_ID", referencedColumnName = "ACTIVITY_ID")
    @ManyToOne(optional = false)
    private Activity activityId;
    @JoinColumn(name = "BOOKING_TYPE", referencedColumnName = "BOOKING_ID")
    @ManyToOne(optional = false)
    private BookingType bookingType;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users userId;

    public ActivtyBooking() {
    }

    public ActivtyBooking(Long activityBookingId) {
        this.activityBookingId = activityBookingId;
    }

    public ActivtyBooking(Long activityBookingId, String transactionName, short numOfPeople, BigDecimal activityCost, Date dateOfBooking) {
        this.activityBookingId = activityBookingId;
        this.transactionName = transactionName;
        this.numOfPeople = numOfPeople;
        this.activityCost = activityCost;
        this.dateOfBooking = dateOfBooking;
    }

    public Long getActivityBookingId() {
        return activityBookingId;
    }

    public void setActivityBookingId(Long activityBookingId) {
        this.activityBookingId = activityBookingId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public short getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(short numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public BigDecimal getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(BigDecimal activityCost) {
        this.activityCost = activityCost;
    }

    public Date getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(Date dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public Activity getActivityId() {
        return activityId;
    }

    public void setActivityId(Activity activityId) {
        this.activityId = activityId;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (activityBookingId != null ? activityBookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActivtyBooking)) {
            return false;
        }
        ActivtyBooking other = (ActivtyBooking) object;
        if ((this.activityBookingId == null && other.activityBookingId != null) || (this.activityBookingId != null && !this.activityBookingId.equals(other.activityBookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.ActivtyBooking[ activityBookingId=" + activityBookingId + " ]";
    }
    
}
