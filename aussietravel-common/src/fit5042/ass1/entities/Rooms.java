/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "ROOMS")
@XmlRootElement
@NamedQueries({
     @NamedQuery(name = "Rooms.findAll", query = "SELECT r FROM Rooms r")
    , @NamedQuery(name = "Rooms.findByRoomId", query = "SELECT r FROM Rooms r WHERE r.roomId = :roomId")
    , @NamedQuery(name = "Rooms.findByRoomName", query = "SELECT r FROM Rooms r WHERE r.roomName = :roomName")
    , @NamedQuery(name = "Rooms.findByRoomOccupancy", query = "SELECT r FROM Rooms r WHERE r.roomOccupancy = :roomOccupancy")
    , @NamedQuery(name = "Rooms.findByRoomCost", query = "SELECT r FROM Rooms r WHERE r.roomCost = :roomCost")})
public class Rooms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROOM_ID")
    private Long roomId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ROOM_NAME")
    private String roomName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROOM_OCCUPANCY")
    private short roomOccupancy;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROOM_COST")
    private BigDecimal roomCost;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roomType")
    private Collection<ResortBooking> resortBookingCollection;

    public Rooms() {
    }

    public Rooms(Long roomId) {
        this.roomId = roomId;
    }

    public Rooms(Long roomId, String roomName, short roomOccupancy, BigDecimal roomCost) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.roomOccupancy = roomOccupancy;
        this.roomCost = roomCost;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public short getRoomOccupancy() {
        return roomOccupancy;
    }

    public void setRoomOccupancy(short roomOccupancy) {
        this.roomOccupancy = roomOccupancy;
    }

    public BigDecimal getRoomCost() {
        return roomCost;
    }

    public void setRoomCost(BigDecimal roomCost) {
        this.roomCost = roomCost;
    }

    @XmlTransient
    public Collection<ResortBooking> getResortBookingCollection() {
        return resortBookingCollection;
    }

    public void setResortBookingCollection(Collection<ResortBooking> resortBookingCollection) {
        this.resortBookingCollection = resortBookingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roomId != null ? roomId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rooms)) {
            return false;
        }
        Rooms other = (Rooms) object;
        if ((this.roomId == null && other.roomId != null) || (this.roomId != null && !this.roomId.equals(other.roomId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.Rooms[ roomId=" + roomId + " ]";
    }
    
}
