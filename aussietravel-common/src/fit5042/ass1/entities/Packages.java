/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "PACKAGES")
@XmlRootElement
@NamedQueries({
   @NamedQuery(name = "Packages.findAll", query = "SELECT p FROM Packages p")
    , @NamedQuery(name = "Packages.findByPackageId", query = "SELECT p FROM Packages p WHERE p.packageId = :packageId")
    , @NamedQuery(name = "Packages.findByPackageName", query = "SELECT p FROM Packages p WHERE p.packageName = :packageName")
    , @NamedQuery(name = "Packages.findByPackageCost", query = "SELECT p FROM Packages p WHERE p.packageCost = :packageCost")
})
public class Packages implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PACKAGE_ID")
    private Long packageId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "PACKAGE_NAME")
    private String packageName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PACKAGE_COST")
    private BigDecimal packageCost;
    @JoinColumn(name = "SCHEDULED_ACTIVITY_3", referencedColumnName = "ACTIVITY_ID")
    @ManyToOne
    private Activity scheduledActivity3;
    @JoinColumn(name = "SCHEDULED_ACTIVITY_2", referencedColumnName = "ACTIVITY_ID")
    @ManyToOne
    private Activity scheduledActivity2;
    @JoinColumn(name = "SCHEDULED_ACTIVITY_1", referencedColumnName = "ACTIVITY_ID")
    @ManyToOne
    private Activity scheduledActivity1;
    @JoinColumn(name = "ASSOCIATED_RESORT", referencedColumnName = "RESORT_ID")
    @ManyToOne(optional = false)
    private Resorts associatedResort;
    @OneToMany(mappedBy = "packageId")
    private Collection<PackageBooking> packageBookingCollection;

    public Packages() {
    }

    public Packages(Long packageId) {
        this.packageId = packageId;
    }

    public Packages(Long packageId, String packageName, BigDecimal packageCost) {
        this.packageId = packageId;
        this.packageName = packageName;
        this.packageCost = packageCost;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public BigDecimal getPackageCost() {
        return packageCost;
    }

    public void setPackageCost(BigDecimal packageCost) {
        this.packageCost = packageCost;
    }

    public Activity getScheduledActivity3() {
        return scheduledActivity3;
    }

    public void setScheduledActivity3(Activity scheduledActivity3) {
        this.scheduledActivity3 = scheduledActivity3;
    }

    public Activity getScheduledActivity2() {
        return scheduledActivity2;
    }

    public void setScheduledActivity2(Activity scheduledActivity2) {
        this.scheduledActivity2 = scheduledActivity2;
    }

    public Activity getScheduledActivity1() {
        return scheduledActivity1;
    }

    public void setScheduledActivity1(Activity scheduledActivity1) {
        this.scheduledActivity1 = scheduledActivity1;
    }

    public Resorts getAssociatedResort() {
        return associatedResort;
    }

    public void setAssociatedResort(Resorts associatedResort) {
        this.associatedResort = associatedResort;
    }

    @XmlTransient
    public Collection<PackageBooking> getPackageBookingCollection() {
        return packageBookingCollection;
    }

    public void setPackageBookingCollection(Collection<PackageBooking> packageBookingCollection) {
        this.packageBookingCollection = packageBookingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (packageId != null ? packageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Packages)) {
            return false;
        }
        Packages other = (Packages) object;
        if ((this.packageId == null && other.packageId != null) || (this.packageId != null && !this.packageId.equals(other.packageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.Packages[ packageId=" + packageId + " ]";
    }
    
}
