/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "PACKAGE_BOOKING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PackageBooking.findAll", query = "SELECT p FROM PackageBooking p")
    , @NamedQuery(name = "PackageBooking.findByPackageBookingId", query = "SELECT p FROM PackageBooking p WHERE p.packageBookingId = :packageBookingId")
    , @NamedQuery(name = "PackageBooking.findByTransactionName", query = "SELECT p FROM PackageBooking p WHERE p.transactionName = :transactionName")
    , @NamedQuery(name = "PackageBooking.findByNumOfPeople", query = "SELECT p FROM PackageBooking p WHERE p.numOfPeople = :numOfPeople")
    , @NamedQuery(name = "PackageBooking.findByPackageCost", query = "SELECT p FROM PackageBooking p WHERE p.packageCost = :packageCost")
    , @NamedQuery(name = "PackageBooking.findByDateOfBooking", query = "SELECT p FROM PackageBooking p WHERE p.dateOfBooking = :dateOfBooking")
    , @NamedQuery(name = "PackageBooking.findByType", query = "SELECT p FROM PackageBooking p WHERE p.bookingType  = :bookingType")})
public class PackageBooking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PACKAGE_BOOKING_ID")
    private Long packageBookingId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TRANSACTION_NAME")
    private String transactionName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_OF_PEOPLE")
    private short numOfPeople;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PACKAGE_COST")
    private BigDecimal packageCost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_OF_BOOKING")
    @Temporal(TemporalType.DATE)
    private Date dateOfBooking;
    @JoinColumn(name = "BOOKING_TYPE", referencedColumnName = "BOOKING_ID")
    @ManyToOne(optional = false)
    private BookingType bookingType;
    @JoinColumn(name = "PACKAGE_ID", referencedColumnName = "PACKAGE_ID")
    @ManyToOne
    private Packages packageId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users userId;

    public PackageBooking() {
    }

    public PackageBooking(Long packageBookingId) {
        this.packageBookingId = packageBookingId;
    }

    public PackageBooking(Long packageBookingId, String transactionName, short numOfPeople, BigDecimal packageCost, Date dateOfBooking) {
        this.packageBookingId = packageBookingId;
        this.transactionName = transactionName;
        this.numOfPeople = numOfPeople;
        this.packageCost = packageCost;
        this.dateOfBooking = dateOfBooking;
    }

    public Long getPackageBookingId() {
        return packageBookingId;
    }

    public void setPackageBookingId(Long packageBookingId) {
        this.packageBookingId = packageBookingId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public short getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(short numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public BigDecimal getPackageCost() {
        return packageCost;
    }

    public void setPackageCost(BigDecimal packageCost) {
        this.packageCost = packageCost;
    }

    public Date getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(Date dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public Packages getPackageId() {
        return packageId;
    }

    public void setPackageId(Packages packageId) {
        this.packageId = packageId;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (packageBookingId != null ? packageBookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PackageBooking)) {
            return false;
        }
        PackageBooking other = (PackageBooking) object;
        if ((this.packageBookingId == null && other.packageBookingId != null) || (this.packageBookingId != null && !this.packageBookingId.equals(other.packageBookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.PackageBooking[ packageBookingId=" + packageBookingId + " ]";
    }
    
}
