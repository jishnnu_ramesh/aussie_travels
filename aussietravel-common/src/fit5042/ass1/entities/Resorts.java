/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "RESORTS")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Resorts.findAll", query = "SELECT r FROM Resorts r")
    , @NamedQuery(name = "Resorts.findByResortId", query = "SELECT r FROM Resorts r WHERE r.resortId = :resortId")
    , @NamedQuery(name = "Resorts.findByResortName", query = "SELECT r FROM Resorts r WHERE r.resortName = :resortName")
    , @NamedQuery(name = "Resorts.findByStreetAddress", query = "SELECT r FROM Resorts r WHERE r.streetAddress = :streetAddress")
    , @NamedQuery(name = "Resorts.findByCity", query = "SELECT r FROM Resorts r WHERE r.city = :city")})
public class Resorts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RESORT_ID")
    private Long resortId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "RESORT_NAME")
    private String resortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "STREET_ADDRESS")
    private String streetAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "CITY")
    private String city;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "resortId")
    private Collection<ResortBooking> resortBookingCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "associatedResort")
    private Collection<Packages> packagesCollection;

    public Resorts() {
    }

    public Resorts(Long resortId) {
        this.resortId = resortId;
    }

    public Resorts(Long resortId, String resortName, String streetAddress, String city) {
        this.resortId = resortId;
        this.resortName = resortName;
        this.streetAddress = streetAddress;
        this.city = city;
    }

    public Long getResortId() {
        return resortId;
    }

    public void setResortId(Long resortId) {
        this.resortId = resortId;
    }

    public String getResortName() {
        return resortName;
    }

    public void setResortName(String resortName) {
        this.resortName = resortName;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @XmlTransient
    public Collection<ResortBooking> getResortBookingCollection() {
        return resortBookingCollection;
    }

    public void setResortBookingCollection(Collection<ResortBooking> resortBookingCollection) {
        this.resortBookingCollection = resortBookingCollection;
    }

    @XmlTransient
    public Collection<Packages> getPackagesCollection() {
        return packagesCollection;
    }

    public void setPackagesCollection(Collection<Packages> packagesCollection) {
        this.packagesCollection = packagesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resortId != null ? resortId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resorts)) {
            return false;
        }
        Resorts other = (Resorts) object;
        if ((this.resortId == null && other.resortId != null) || (this.resortId != null && !this.resortId.equals(other.resortId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.Resorts[ resortId=" + resortId + " ]";
    }
    
}
