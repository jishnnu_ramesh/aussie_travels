/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({
       @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findByUserId", query = "SELECT u FROM Users u WHERE u.userId = :userId")
    , @NamedQuery(name = "Users.findByLastName", query = "SELECT u FROM Users u WHERE u.lastName = :lastName")
    , @NamedQuery(name = "Users.findByFirstName", query = "SELECT u FROM Users u WHERE u.firstName = :firstName")
    , @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")
    , @NamedQuery(name = "Users.findByStreetAddress", query = "SELECT u FROM Users u WHERE u.streetAddress = :streetAddress")
    , @NamedQuery(name = "Users.findByPhoneNumber", query = "SELECT u FROM Users u WHERE u.phoneNumber = :phoneNumber")
     , @NamedQuery(name = "Users.findByIdAndFirstName", query = "SELECT u FROM Users u WHERE u.firstName = :firstName AND u.userId = :userId")
    , @NamedQuery(name = "Users.findByIdAndLastName", query = "SELECT u FROM Users u WHERE u.lastName = :lastName AND u.userId = :userId")
    , @NamedQuery(name = "Users.findByIdAndEmail", query = "SELECT u FROM Users u WHERE u.email= :email AND u.userId = :userId ")
    , @NamedQuery(name = "Users.findByIdAndType", query = "SELECT u FROM Users u WHERE u.userType.id = :userType AND u.userId = :userId ")
    , @NamedQuery(name = "Users.findByFirstNameAndLastName", query = "SELECT u FROM Users u WHERE u.firstName = :firstName AND u.lastName = :lastName")
    , @NamedQuery(name = "Users.findByFirstNameAndEmail", query = "SELECT u FROM Users u WHERE u.firstName = :firstName AND u.email = :email")
    , @NamedQuery(name = "Users.findByFirstNameAndType", query = "SELECT u FROM Users u WHERE u.firstName = :firstName AND u.userType.id = :userType ")
    , @NamedQuery(name = "Users.findByLastNameAndEmail", query = "SELECT u FROM Users u WHERE u.lastName = :lastName AND u.email = :email ")
    , @NamedQuery(name = "Users.findByLastNameAndType", query = "SELECT u FROM Users u WHERE u.lastName = :lastName AND u.userType.id = :userType ")
    , @NamedQuery(name = "Users.findByTypeAndEmail", query = "SELECT u FROM Users u WHERE u.userType.id = :userType AND u.email = :email ")
   
})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ID")
    private Long userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FIRST_NAME")
    private String firstName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "PASSWORD")
    private String password;
    @Size(max = 300)
    @Column(name = "STREET_ADDRESS")
    private String streetAddress;
    @Size(max = 10)
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;
    @OneToMany(mappedBy = "userId")
    private Collection<ResortBooking> resortBookingCollection;
    @OneToMany(mappedBy = "userId")
    private Collection<ActivtyBooking> activtyBookingCollection;
    @JoinColumn(name = "USER_TYPE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private UserType userType;
    @OneToMany(mappedBy = "userId")
    private Collection<PackageBooking> packageBookingCollection;

    public Users() {
    }

    public Users(Long userId) {
        this.userId = userId;
    }

    public Users(Long userId, String lastName, String firstName, String email, String password) {
        this.userId = userId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @XmlTransient
    public Collection<ResortBooking> getResortBookingCollection() {
        return resortBookingCollection;
    }

    public void setResortBookingCollection(Collection<ResortBooking> resortBookingCollection) {
        this.resortBookingCollection = resortBookingCollection;
    }

    @XmlTransient
    public Collection<ActivtyBooking> getActivtyBookingCollection() {
        return activtyBookingCollection;
    }

    public void setActivtyBookingCollection(Collection<ActivtyBooking> activtyBookingCollection) {
        this.activtyBookingCollection = activtyBookingCollection;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @XmlTransient
    public Collection<PackageBooking> getPackageBookingCollection() {
        return packageBookingCollection;
    }

    public void setPackageBookingCollection(Collection<PackageBooking> packageBookingCollection) {
        this.packageBookingCollection = packageBookingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.Users[ userId=" + userId + " ]";
    }
    
}
