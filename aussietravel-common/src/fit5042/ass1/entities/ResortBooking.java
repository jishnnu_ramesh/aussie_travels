/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jishnu
 */
@Entity
@Table(name = "RESORT_BOOKING")
@XmlRootElement
@NamedQueries({
         @NamedQuery(name = "ResortBooking.findAll", query = "SELECT r FROM ResortBooking r")
    , @NamedQuery(name = "ResortBooking.findByResortBookingId", query = "SELECT r FROM ResortBooking r WHERE r.resortBookingId = :resortBookingId")
    , @NamedQuery(name = "ResortBooking.findByTransactionName", query = "SELECT r FROM ResortBooking r WHERE r.transactionName = :transactionName")
    , @NamedQuery(name = "ResortBooking.findByNumOfPeople", query = "SELECT r FROM ResortBooking r WHERE r.numOfPeople = :numOfPeople")
    , @NamedQuery(name = "ResortBooking.findByNumOfRooms", query = "SELECT r FROM ResortBooking r WHERE r.numOfRooms = :numOfRooms")
    , @NamedQuery(name = "ResortBooking.findByAddOnService", query = "SELECT r FROM ResortBooking r WHERE r.addOnService = :addOnService")
    , @NamedQuery(name = "ResortBooking.findByTotalCost", query = "SELECT r FROM ResortBooking r WHERE r.totalCost = :totalCost")
    , @NamedQuery(name = "ResortBooking.findByDateOfBooking", query = "SELECT r FROM ResortBooking r WHERE r.dateOfBooking = :dateOfBooking")
    , @NamedQuery(name = "ResortBooking.findByType", query="Select r FROM ResortBooking r WHERE r.bookingType = :bookingType")})
public class ResortBooking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RESORT_BOOKING_ID")
    private Long resortBookingId;
    @Size(max = 50)
    @Column(name = "TRANSACTION_NAME")
    private String transactionName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_OF_PEOPLE")
    private short numOfPeople;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_OF_ROOMS")
    private short numOfRooms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADD_ON_SERVICE")
    private Boolean addOnService;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_COST")
    private BigDecimal totalCost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_OF_BOOKING")
    @Temporal(TemporalType.DATE)
    private Date dateOfBooking;
    @JoinColumn(name = "BOOKING_TYPE", referencedColumnName = "BOOKING_ID")
    @ManyToOne(optional = false)
    private BookingType bookingType;
    @JoinColumn(name = "RESORT_ID", referencedColumnName = "RESORT_ID")
    @ManyToOne(optional = false)
    private Resorts resortId;
    @JoinColumn(name = "ROOM_TYPE", referencedColumnName = "ROOM_ID")
    @ManyToOne(optional = false)
    private Rooms roomType;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users userId;

    public ResortBooking() {
    }

    public ResortBooking(Long resortBookingId) {
        this.resortBookingId = resortBookingId;
    }

    public ResortBooking(Long resortBookingId, short numOfPeople, short numOfRooms, Boolean addOnService, BigDecimal totalCost, Date dateOfBooking) {
        this.resortBookingId = resortBookingId;
        this.numOfPeople = numOfPeople;
        this.numOfRooms = numOfRooms;
        this.addOnService = addOnService;
        this.totalCost = totalCost;
        this.dateOfBooking = dateOfBooking;
    }

    public Long getResortBookingId() {
        return resortBookingId;
    }

    public void setResortBookingId(Long resortBookingId) {
        this.resortBookingId = resortBookingId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public short getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(short numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public short getNumOfRooms() {
        return numOfRooms;
    }

    public void setNumOfRooms(short numOfRooms) {
        this.numOfRooms = numOfRooms;
    }

    public Boolean getAddOnService() {
        return addOnService;
    }

    public void setAddOnService(Boolean addOnService) {
        this.addOnService = addOnService;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public Date getDateOfBooking() {
        return dateOfBooking;
    }

    public void setDateOfBooking(Date dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public Resorts getResortId() {
        return resortId;
    }

    public void setResortId(Resorts resortId) {
        this.resortId = resortId;
    }

    public Rooms getRoomType() {
        return roomType;
    }

    public void setRoomType(Rooms roomType) {
        this.roomType = roomType;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resortBookingId != null ? resortBookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResortBooking)) {
            return false;
        }
        ResortBooking other = (ResortBooking) object;
        if ((this.resortBookingId == null && other.resortBookingId != null) || (this.resortBookingId != null && !this.resortBookingId.equals(other.resortBookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fit5042.ass1.entities.ResortBooking[ resortBookingId=" + resortBookingId + " ]";
    }
    
}
