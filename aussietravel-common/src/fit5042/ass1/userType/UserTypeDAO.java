/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.ass1.userType;

import fit5042.ass1.entities.UserType;
import javax.ejb.Remote;

/**
 *
 * @author Jishnu
 */
@Remote
public interface UserTypeDAO {
    
    
    public UserType getUserType(String id);
    
}
